# Test Result Viewer - Viewer Component

This repository is part of the Test Result Viewer application.

The *Viewer* is a Web Frontend to analyze and view test results.

It consist of two parts:
1. Frontend, Angular
2. Reverse Proxy, nginx 

## How to run for production

Build the Angular App to be served statically. With *angular cli* installed (see below), change into the frontend directory and run 
```bash
ng build
```
The provided nginx proxy configuration will serve the resulting folder `frontend/dist/angular-frontend` automatically on port 443. Requests to `/api` are forwarded to port 9200 on the docker host, `/kibana` to port 5601. See elasticsearch repository for more instructions.

To run the nginx proxy, use 
```bash
docker compose up
```
inside the `viewer` folder. 

## How to run for development

The provided docker-compose.dev.yml uses Caddy as proxy, which is easier in configuration. Use `caddy/Caddyfile` to forward elastic requests to an instance running locally on port 9200, or `caddy/Caddyfile_server` to forward `/api` directly to our development server at OST. 

Rename the files accordingly, the docker container will look for `caddy/Caddyfile`.

To start Caddy as the proxy, run
```sh
docker compose -f compose.dev.yaml up
```

To run the angular application for development (changes are represented immediately), change into the frontend folder and run

```sh
npm install
ng serve --watch --host "0.0.0.0" --disable-host-check
```

Open the application on http://localhost:9080.

## Dev Tools

These tools are needed to develop the angular application.

### Node.js 

See https://nodejs.org/en/download for Windows, or the following instructions for Linux machines.

To install Node Version Manager (nvm) 

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```

Restart your shell. 

Then install the newest LTS version of nodejs with

```bash
nvm install --lts
```

This currently results in installing node 18.15.0 and npm 9.5.0.

### Angular CLI

With node and npm installed, use the command 

```bash
npm install --global @angular/cli
```

to install the angular cli application `ng`. This tool was developed using `ng` version 15.2. version 15.2.6.
