import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';
import { By } from '@angular/platform-browser';

import { SnackbarErrorComponent } from './snackbar-error.component';

describe('SnackbarErrorComponent', () => {
  let component: SnackbarErrorComponent;
  let fixture: ComponentFixture<SnackbarErrorComponent>;
  let de: DebugElement;

  let data: string;
  let snackbarRef: any;

  beforeEach(async () => {
    data = 'some error message';
    snackbarRef = {};

    await TestBed.configureTestingModule({
      declarations: [SnackbarErrorComponent],
      providers: [
        { provide: MAT_SNACK_BAR_DATA, useValue: data },
        { provide: MatSnackBarRef, useValue: snackbarRef },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SnackbarErrorComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display provided data as message', () => {
    expect(de.query(By.css('span')).nativeElement.innerText.trim()).toBe(
      'some error message'
    );
  });
});
