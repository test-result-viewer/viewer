import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';
import { HttpErrorResponse } from '@angular/common/http';
import * as Utilities from './utilities';

describe('Utilities', () => {
  describe('getSymbol', () => {
    it('should return ✓ when `success`', () => {
      expect(Utilities.getSymbol('success')).toBe('✓');
    });

    it('should return ! when `failure`', () => {
      expect(Utilities.getSymbol('failure')).toBe('!');
    });

    it('should return ⨯ when `error`', () => {
      expect(Utilities.getSymbol('error')).toBe('⨯');
    });

    it('should return I when `ignored`', () => {
      expect(Utilities.getSymbol('ignored')).toBe('I');
    });

    it('should return ? when `inconclusive`', () => {
      expect(Utilities.getSymbol('inconclusive')).toBe('?');
    });

    it('should return ? otherwise', () => {
      expect(Utilities.getSymbol('asdf')).toBe('?');
    });
  });

  describe('isAllError', () => {
    it('should return `error` when everything is `error`', () => {
      const hits: Hit<Testresult>[] = [
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
      ];
      expect(Utilities.isAllError(hits)).toBe('error');
    });

    it('should return `success` when at least one is not `error`', () => {
      const hits: Hit<Testresult>[] = [
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'success',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'ignored',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
          },
        },
      ];
      expect(Utilities.isAllError(hits)).toBe('success');
    });

    it("should return '' when array is `null` or `undefined`", () => {
      expect(Utilities.isAllError(null)).toBe('');
      expect(Utilities.isAllError(undefined)).toBe('');
    });

    it("should return '' when array is empty", () => {
      expect(Utilities.isAllError([])).toBe('');
    });

    it("should return '' when no result was found", () => {
      expect(
        Utilities.isAllError([
          {
            _index: '',
            _id: '',
            _score: '',
            _source: {},
          },
        ])
      ).toBe('');
    });
  });

  describe('getHTTPErrorAsString', () => {
    it('should return HTTPError as a string formatted', () => {
      expect(
        Utilities.getHTTPErrorAsString(
          new HttpErrorResponse({
            status: 404,
            statusText: 'Not found',
          })
        )
      ).toEqual('404 - Http failure response for (unknown url): 404 Not found');
    });
  });

  describe('getErrorMessage', () => {
    it('should return Error as a string formatted', () => {
      expect(Utilities.getErrorMessage(new Error('some error'))).toEqual(
        'some error'
      );
    });

    it('should return objects and number as string formatted', () => {
      expect(Utilities.getErrorMessage(123)).toEqual('123');
      expect(Utilities.getErrorMessage({})).toEqual('[object Object]');
    });
  });
});
