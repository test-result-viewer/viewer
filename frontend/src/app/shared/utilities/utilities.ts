import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarErrorComponent } from '../snackbar/snackbar-error/snackbar-error.component';

// TODO: Replace string with type result = success | failed | ...
export function getSymbol(result: string | undefined): string {
  switch (result) {
    case 'success':
      return '✓';
    case 'failure':
      return '!';
    case 'error':
      return '⨯';
    case 'ignored':
      return 'I';
    case 'inconclusive':
      return '?';
    default:
      return '?';
  }
}

export function isAllSuccess(
  data: Hit<Testresult>[] | undefined | null
): string {
  if (!data || data.length == 0) return '';

  const x =
    data.reduce((total, hit) => {
      return total._source.result === 'success' ? hit : total;
    })._source.result || '';

  return x === 'success' ? x : 'error';
}

export function isAllError(
  data: Hit<Testresult>[] | undefined | null
): string {
  if (!data || data.length == 0) return '';

  const x = data.reduce((total, hit) => {
    return total._source.result === 'error' ? hit: total;
  })._source.result || '';
  
  if (x === '') return '';
  return x === 'error' ? x : 'success';
}

export function getHTTPErrorAsString(errorResponse: HttpErrorResponse): string {
  return `${errorResponse.status} - ${errorResponse.message}`;
}

export function getErrorMessage(error: unknown): string {
  if (error instanceof Error) return error.message;
  else return String(error);
}

export function toTitleCase(str: string | undefined): string {
  if (str && str.length > 0)
    return str
      .split('-')
      .map(w => w[0].toUpperCase() + w.substring(1).toLowerCase())
      .join(' ');
  else return '';
}

export function dateToYYYYMMDDHHMMSS(date: Date): string {
  const year = date.toLocaleString('default', { year: 'numeric' });
  const month = date.toLocaleString('default', { month: '2-digit' });
  const day = date.toLocaleString('default', { day: '2-digit' });
  const hour = date.toLocaleString('default', {
    hour: '2-digit',
    hour12: false,
  });
  const minute = date.toLocaleString('default', { minute: '2-digit' });
  const second = date.toLocaleString('default', { second: '2-digit' });

  return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

export function createArrayWithSize(size: number, defaultValue: unknown) {
  return Array(size).fill(defaultValue);
}

export function errorSnackBar(snackBar: MatSnackBar, message: string) {
  const ms = 10000;
  snackBar.openFromComponent(
    SnackbarErrorComponent,
    {
      data: message,
      duration: ms
    }
  )
}

export function infoSnackBar(snackBar: MatSnackBar, message: string) {
  const ms = 3000;
  snackBar.openFromComponent(
    SnackbarErrorComponent,
    {
      data: message,
      duration: ms
    }
  )
}

export function dismissibleSnackBar(snackBar: MatSnackBar, message: string) {
  snackBar.openFromComponent(
    SnackbarErrorComponent,
    {
      data: message
    }
  )
}
