import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  IndexResponse,
  UpdateResponse,
} from 'src/app/types/elasticsearch.types';
import { LogService } from '../logs/log.service';

export const MAX_RESULT_SIZE = 10_000;
export const MAX_AGGREGATION_SIZE = 100;
export const MAX_TOP_HITS_SIZE = 100;
export const MAX_BUCKET_SIZE = 65_536;

@Injectable({
  providedIn: 'root',
})
export class ElasticsearchService {
  private static DB_URL = '/api';

  constructor(private http: HttpClient, private logger: LogService) {}

  private getAuthorizationHeader(): string {
    return (
      'Basic ' + btoa(`${environment.ELASTIC_USER}:${environment.ELASTIC_PASS}`)
    );
  }

  search<T>(index: string, query: unknown): Observable<T> {
    const url = `${ElasticsearchService.DB_URL}/${index}/_search`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.getAuthorizationHeader(),
      }),
    };
    this.logger.debug('Elastic Query sent: ', query);
    const result = this.http.post<T>(url, query, httpOptions);

    if (result instanceof HttpErrorResponse) {
      this.logger.error(result.error);
      throw result;
    } else {
      return result;
    }
  }

  templateSearch<T>(index: string, templateId: string, params: unknown) {
    const url = `${ElasticsearchService.DB_URL}/${index}/_search/template`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.getAuthorizationHeader(),
      }),
    };
    this.logger.debug('Elastic Search Template: ', templateId);
    this.logger.debug('Elastic Parameters: ', params);

    const query = { id: templateId, params };
    const result = this.http.post<T>(url, query, httpOptions);
    if (result instanceof HttpErrorResponse) {
      this.logger.error(result.error);
      throw result;
    } else {
      return result;
    }
  }

  getById<T>(index: string, id: string): Observable<T> {
    const url = `${ElasticsearchService.DB_URL}/${index}/_doc/${id}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.getAuthorizationHeader(),
      }),
    };

    const result = this.http.get<T>(url, httpOptions);
    if (result instanceof HttpErrorResponse) {
      this.logger.error(result.error);
      throw result;
    } else {
      return result;
    }
  }

  update(index: string, id: string, doc: unknown): Observable<UpdateResponse> {
    const url = `${ElasticsearchService.DB_URL}/${index}/_update/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.getAuthorizationHeader(),
      }),
    };

    const result = this.http.post<UpdateResponse>(url, doc, httpOptions);
    if (result instanceof HttpErrorResponse) {
      this.logger.error(result.error);
      throw result;
    } else {
      return result;
    }
  }

  create(index: string, doc: unknown): Observable<UpdateResponse> {
    const url = `${ElasticsearchService.DB_URL}/${index}/_doc/`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.getAuthorizationHeader(),
      }),
    };

    const result = this.http.post<UpdateResponse>(url, doc, httpOptions);
    if (result instanceof HttpErrorResponse) {
      this.logger.error(result.error);
      throw result;
    } else {
      return result;
    }
  }

  getAllIndices(): Observable<IndexResponse> {
    const url = `${ElasticsearchService.DB_URL}/_all`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.getAuthorizationHeader(),
      }),
    };

    const result = this.http.get<IndexResponse>(url, httpOptions);
    if (result instanceof HttpErrorResponse) {
      this.logger.error(result.error);
      throw result;
    } else {
      return result;
    }
  }
}
