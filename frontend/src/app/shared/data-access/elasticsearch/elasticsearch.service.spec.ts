import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed, getTestBed } from '@angular/core/testing';
import { ElasticsearchService } from './elasticsearch.service';

describe('ElasticsearchService', () => {
  let injector: TestBed;
  let service: ElasticsearchService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ElasticsearchService],
    });

    injector = getTestBed();
    service = injector.get(ElasticsearchService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call search api', () => {
    service.search('testresult', {}).subscribe(r => {});

    const req = httpMock.expectOne('/api/testresult/_search');
    expect(req.request.method).toBe('POST');
    expect(req.request.headers.get('Content-Type')).toBe('application/json');
    expect(req.request.headers.get('Authorization')).toBeTruthy();
    req.flush({});
  });

  it('should call doc api', () => {
    service.getById('testresult', '2').subscribe(r => {});

    const req = httpMock.expectOne('/api/testresult/_doc/2');
    expect(req.request.method).toBe('GET');
    expect(req.request.headers.get('Content-Type')).toBe('application/json');
    expect(req.request.headers.get('Authorization')).toBeTruthy();
    req.flush({});
  });

  it('should call update api', () => {
    service.update('testresult', '2', {}).subscribe(r => {});

    const req = httpMock.expectOne('/api/testresult/_update/2');
    expect(req.request.method).toBe('POST');
    expect(req.request.headers.get('Content-Type')).toBe('application/json');
    expect(req.request.headers.get('Authorization')).toBeTruthy();
    req.flush({});
  });
});
