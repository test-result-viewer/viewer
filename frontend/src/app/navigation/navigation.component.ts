import { Component } from '@angular/core';
import * as utils from '../shared/utilities/utilities';
import { routes } from '../app-routing.module';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  isExpanded = true;

  pages = routes
    .filter(route => route.path)
    .map(route => route.path)
    .filter(path => path != 'testresult-detail')
    .filter(path => path != '**');

  toTitleCase(str: string | undefined) {
    return utils.toTitleCase(str);
  }
}
