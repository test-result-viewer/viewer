import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCardComponent } from './dashboard-card.component';
import { By } from '@angular/platform-browser';

describe('DashboardCardComponent', () => {
  let component: DashboardCardComponent;
  let fixture: ComponentFixture<DashboardCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardCardComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the title as card title', () => {
    component.title = 'my title';
    fixture.detectChanges();

    expect(
      fixture.debugElement
        .query(By.css('mat-card-title'))
        .nativeElement.innerText.trim()
    ).toEqual('my title');
  });

  it('should set the content as card content', () => {
    component.content = 'a awsome content';
    fixture.detectChanges();

    expect(
      fixture.debugElement
        .query(By.css('mat-card-content'))
        .nativeElement.innerText.trim()
    ).toEqual('a awsome content');
  });
});
