import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardCardComponent } from './dashboard-card.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [DashboardCardComponent],
  imports: [CommonModule, MatCardModule],
  exports: [DashboardCardComponent],
})
export class DashboardCardModule {}
