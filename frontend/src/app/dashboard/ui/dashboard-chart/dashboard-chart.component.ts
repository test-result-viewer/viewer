import { Component, Input } from '@angular/core';
import { ChartOptions, ChartConfiguration } from 'chart.js';

@Component({
  selector: 'app-dashboard-chart',
  templateUrl: './dashboard-chart.component.html',
  styleUrls: ['./dashboard-chart.component.scss'],
})
export class DashboardChartComponent {
  @Input() data: ChartConfiguration<'bar'>['data'] = {
    labels: [],
    datasets: [],
  };
  @Input() chartOptions?: ChartOptions<'bar'>;
  @Input() chartLegend = true;
}
