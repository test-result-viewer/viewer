import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgChartsModule } from 'ng2-charts';
import { DashboardChartComponent } from './dashboard-chart.component';

@NgModule({
  declarations: [DashboardChartComponent],
  imports: [CommonModule, NgChartsModule],
  exports: [DashboardChartComponent],
})
export class DashboardChartModule {}
