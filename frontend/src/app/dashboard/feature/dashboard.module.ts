import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardCardModule } from '../ui/dashboard-card/dashboard-card.module';
import { DashboardChartModule } from '../ui/dashboard-chart/dashboard-chart.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    DashboardCardModule,
    DashboardChartModule,
    MatSnackBarModule,
  ],
  exports: [DashboardComponent],
})
export class DashboardModule {}
