import { Component, OnInit } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import {
  DashboardService,
  Dataset,
  ErrorHistory,
  LastRun,
} from '../data-access/dashboard.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { errorSnackBar, getHTTPErrorAsString } from '../../shared/utilities/utilities'
import { HttpErrorResponse } from '@angular/common/http';

const EMPTY_LAST_RUN = {
  date: new Date(0),
  success: -1,
  ignored: -1,
  error: -1,
  inconclusive: -1,
  failure: -1,
};

const EMPTY_FAILURES = {
  labels: [],
  datasets: [],
};

@Component({
  selector: 'app-feature',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  lastRun$: Observable<LastRun> = of(EMPTY_LAST_RUN);
  errors$: Observable<{ labels: string[]; datasets: Dataset[] }> =
    of(EMPTY_FAILURES);
  chartOptions = {
    responsive: true,
    plugins: {
      title: {
        display: true,
        text: 'Number of failures',
      },
    },
  };
  chartLegend = true;

  constructor(
    private service: DashboardService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.lastRun$ = this.getLastRun();
    this.errors$ = this.getErrors();
  }

  getLastRun(): Observable<LastRun> {
    return this.service.getLastRun().pipe(
      catchError((err: HttpErrorResponse) => {
        errorSnackBar(this.snackBar, getHTTPErrorAsString(err));
        return of(EMPTY_LAST_RUN);
      })
    );
  }

  getErrors(): Observable<ErrorHistory> {
    return this.service.getTestenvironmentHistory().pipe(
      catchError((err: HttpErrorResponse) => {
        errorSnackBar(this.snackBar, getHTTPErrorAsString(err));
        return of(EMPTY_FAILURES);
      })
    );
  }
}
