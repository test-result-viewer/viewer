import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { DashboardService } from '../data-access/dashboard.service';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { Observable, of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import * as Utilities from 'src/app/shared/utilities/utilities';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarErrorComponent } from 'src/app/shared/snackbar/snackbar-error/snackbar-error.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  let dashboardService: DashboardService;
  let lastRunSpy: jasmine.Spy;
  let testenvironmentHistory: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      providers: [
        { provide: ElasticsearchService, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    dashboardService = fixture.debugElement.injector.get(DashboardService);
    lastRunSpy = spyOn(dashboardService, 'getLastRun').and.returnValue(
      of({
        date: new Date('2023-02-28T20:12:32.000Z'),
        success: 24,
        ignored: 13,
        inconclusive: 3,
        error: 2,
        failure: 3
      })
    );
    testenvironmentHistory = spyOn(
      dashboardService,
      'getTestenvironmentHistory'
    ).and.returnValue(
      of({
        labels: [
          '2023-05-09T22:22:57.000Z',
          '2023-05-09T21:22:57.000Z',
          '2023-02-06T21:22:57.000Z',
        ],
        datasets: [
          { label: 'machine-name-A', data: [0, 0, 0] },
          { label: 'machine-name-B', data: [3, 1, 1] },
        ],
      })
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a last run date card', () => {
    expect(
      fixture.debugElement.query(
        By.css('app-dashboard-card[title="Date / Time"]')
      ).nativeElement.content
    ).toBe(
      Utilities.dateToYYYYMMDDHHMMSS(new Date('2023-02-28T20:12:32.000Z'))
    );
  });

  it('should have a success card with correct number', () => {
    expect(
      fixture.debugElement.query(
        By.css('app-dashboard-card[title="Successful tests"]')
      ).nativeElement.content
    ).toBe(24);
  });

  it('should have a error card with correct number', () => {
    expect(
      fixture.debugElement.query(
        By.css('app-dashboard-card[title="Error tests"]')
      ).nativeElement.content
    ).toBe(2);
  });

  it('should have a inconclusive card with correct number', () => {
    expect(
      fixture.debugElement.query(
        By.css('app-dashboard-card[title="Inconclusive tests"]')
      ).nativeElement.content
    ).toBe(3);
  });
});


describe('DashboardComponent with service error', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  let dashboardService: DashboardService;
  let snackBar: MatSnackBar;

  let lastRunSpy: jasmine.Spy;
  let testenvironmentHistory: jasmine.Spy;
  let snackBarOpen: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      providers: [
        { provide: ElasticsearchService, useValue: {} },
        { provide: MatSnackBar, snackBar },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    dashboardService = fixture.debugElement.injector.get(DashboardService);
    snackBar = fixture.debugElement.injector.get(MatSnackBar);

    snackBarOpen = spyOn(snackBar, 'openFromComponent');
    lastRunSpy = spyOn(dashboardService, 'getLastRun').and.returnValue(throwError(() => { return {status: 503, message: "HTTP Error"}}));
    testenvironmentHistory = spyOn(
      dashboardService,
      'getTestenvironmentHistory'
    ).and.returnValue(throwError(() => { return {status: 503, message: "HTTP Error"}}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open snackbar', () => {
    expect(snackBarOpen.calls.count()).toBe(2);
    expect(snackBarOpen.calls.first().args).toEqual([SnackbarErrorComponent, {data: "503 - HTTP Error", duration: 10_000}])
    expect(snackBarOpen.calls.mostRecent().args).toEqual([SnackbarErrorComponent, {data: "503 - HTTP Error", duration: 10_000}])
  });
});
