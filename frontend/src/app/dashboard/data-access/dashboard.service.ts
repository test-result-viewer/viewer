import { Injectable } from '@angular/core';
import {
  Observable,
  of,
  map,
  switchMap,
  combineLatest,
} from 'rxjs';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { SearchResponse } from 'src/app/types/elasticsearch.types';
import * as Utilities from 'src/app/shared/utilities/utilities';

export const EMPTY_LAST_RUN = {
  date: new Date(0),
  success: -1,
  ignored: -1,
  error: -1,
  inconclusive: -1,
  failed: -1,
};

export const EMPTY_ERRORS = {
  labels: [],
  datasets: [],
};

export interface Dataset {
  label: string;
  data: number[];
}

export interface LastRun {
  date: Date;
  success: number;
  ignored: number;
  inconclusive: number;
  error: number;
  failure: number;
}

export interface ErrorHistory {
  labels: string[];
  datasets: Dataset[];
}

interface DashboardQueryResponse extends SearchResponse {
  aggregations: {
    lastRun: {
      buckets: {
        key: string;
        key_as_string: string;
        doc_count: number;
        result: {
          buckets: {
            key: string;
            doc_count: number;
          }[];
        };
      }[];
    };
  };
}

interface ByHostBucket {
  key: string;
  doc_count: number;
  byDate: {
    buckets: {
      key: number;
      key_as_string: string;
      doc_count: number;
    }[];
  };
}

interface AllHostResponse extends SearchResponse {
  aggregations: {
    allHosts: {
      buckets: {
        key: string;
        doc_count: number;
      }[];
    };
  };
}

interface ErrorQueryResponse extends SearchResponse {
  aggregations: {
    byHost: {
      buckets: ByHostBucket[];
    };
    allDates: {
      buckets: {
        key: number;
        key_as_string: string;
        doc_count: number;
      }[];
    };
  };
}

const TESTRESULT_INDEX = 'testresult';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private elasticsearch: ElasticsearchService) {}

  getLastRun(): Observable<LastRun> {
    return this.elasticsearch
      .templateSearch<DashboardQueryResponse>(
        TESTRESULT_INDEX,
        'get-dashboard',
        {}
      )
      .pipe(
        map(result => {
          const lastRun = result.aggregations.lastRun.buckets[0];

          const data = new Map<string, number>();
          lastRun.result.buckets.forEach(element =>
            data.set(element.key, element.doc_count)
          );

          return {
            date: new Date(lastRun.key_as_string),
            ...Object.fromEntries(data.entries()),
          } as LastRun;
        })
      );
  }

  getTestenvironmentHistory(): Observable<ErrorHistory> {
    return this.elasticsearch
      .templateSearch<AllHostResponse>(TESTRESULT_INDEX, 'get-all-hosts', {})
      .pipe(
        switchMap(hostsRequest =>
          combineLatest([
            this.elasticsearch.templateSearch<ErrorQueryResponse>(
              TESTRESULT_INDEX,
              'get-errors-by-host',
              {}
            ),
            of(
              hostsRequest.aggregations?.allHosts.buckets.map(
                bucket => bucket.key
              )
            ),
          ])
        ),
        map(([response, hosts]) => {
          const labels = response.aggregations?.allDates.buckets.map(
            bucket => bucket.key_as_string
          );
          const datasets = response.aggregations?.byHost.buckets.map(bucket =>
            this.buildChartDataset(bucket, labels)
          );

          const chartLabels = labels
            ? labels?.map(l => Utilities.dateToYYYYMMDDHHMMSS(new Date(l)))
            : [];
          const chartDatasets = hosts
            ? hosts?.map(h => {
                const d = datasets?.find(d => d.label == h);
                return d
                  ? d
                  : {
                      label: h,
                      data: Utilities.createArrayWithSize(labels.length, 0),
                    };
              })
            : [];

          return {
            labels: chartLabels,
            datasets: chartDatasets,
          };
        }),
      );
  }

  private buildChartDataset(bucket: ByHostBucket, labels: string[]): Dataset {
    const label = bucket.key;
    const data: number[] = Utilities.createArrayWithSize(labels?.length, 0);
    bucket['byDate'].buckets.forEach(byDateBucket => {
      const index = labels?.indexOf(byDateBucket.key_as_string);
      if (index === undefined || index === -1) return;
      data[index] = byDateBucket.doc_count;
    });

    return {
      label,
      data,
    };
  }
}
