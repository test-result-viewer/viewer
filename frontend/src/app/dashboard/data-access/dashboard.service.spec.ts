import { TestBed } from '@angular/core/testing';

import { DashboardService } from './dashboard.service';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, of, throwError } from 'rxjs';
import { dateToYYYYMMDDHHMMSS } from 'src/app/shared/utilities/utilities';

describe('DashboardService', () => {
  let service: DashboardService;
  let elasticsearchService: ElasticsearchService;
  let templateSearchSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: {} }],
    });

    function fakeCall<T>(index: string, template: string): Observable<T> {
      if (template === 'get-dashboard') {
        return of({
          took: 2,
          timed_out: false,
          _shards: {
            total: 1,
            successful: 1,
            skipped: 0,
            failed: 0,
          },
          hits: {
            total: {
              value: 211,
              relation: 'eq',
            },
            max_score: null,
            hits: [],
          },
          aggregations: {
            lastRun: {
              doc_count_error_upper_bound: 0,
              sum_other_doc_count: 210,
              buckets: [
                {
                  key: 1684195199000,
                  key_as_string: '2023-05-15T23:59:59.000Z',
                  doc_count: 1,
                  result: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [
                      {
                        key: 'success',
                        doc_count: 24,
                      },
                      {
                        key: 'error',
                        doc_count: 2,
                      },
                      {
                        key: 'ignored',
                        doc_count: 13,
                      },
                      {
                        key: 'inconclusive',
                        doc_count: 2,
                      },
                      {
                        key: 'failure',
                        doc_count: 4
                      }
                    ],
                  },
                },
              ],
            },
          },
        } as T);
      }

      if (template === 'get-all-hosts') {
        return of({
          took: 5,
          timed_out: false,
          _shards: {
            total: 1,
            successful: 1,
            skipped: 0,
            failed: 0,
          },
          hits: {
            total: {
              value: 168,
              relation: 'eq',
            },
            max_score: null,
            hits: [],
          },
          aggregations: {
            allHosts: {
              doc_count_error_upper_bound: 0,
              sum_other_doc_count: 0,
              buckets: [
                {
                  key: 'machine-name-A',
                  doc_count: 42,
                },
                {
                  key: 'machine-name-B',
                  doc_count: 126,
                },
                {
                  key: 'machine-name-C',
                  doc_count: 126,
                },
              ],
            },
          },
        } as T);
      }

      if (template === 'get-errors-by-host') {
        return of({
          took: 1,
          timed_out: false,
          _shards: {
            total: 1,
            successful: 1,
            skipped: 0,
            failed: 0,
          },
          hits: {
            total: {
              value: 5,
              relation: 'eq',
            },
            max_score: null,
            hits: [],
          },
          aggregations: {
            byHost: {
              doc_count_error_upper_bound: 0,
              sum_other_doc_count: 0,
              buckets: [
                {
                  key: 'machine-name-B',
                  doc_count: 5,
                  byDate: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [
                      {
                        key: 1683670977000,
                        key_as_string: '2023-05-09T22:22:57.000Z',
                        doc_count: 3,
                      },
                      {
                        key: 1675718577000,
                        key_as_string: '2023-02-06T21:22:57.000Z',
                        doc_count: 1,
                      },
                      {
                        key: 1683667377000,
                        key_as_string: '2023-05-09T21:22:57.000Z',
                        doc_count: 1,
                      },
                    ],
                  },
                },
                {
                  key: 'machine-name-C',
                  doc_count: 5,
                  byDate: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [
                      {
                        key: 1683670977000,
                        key_as_string: '2023-05-09T21:22:57.000Z',
                        doc_count: 3,
                      },
                      {
                        key: 1675718577000,
                        key_as_string: '2023-02-06T21:22:57.000Z',
                        doc_count: 1,
                      },
                    ],
                  },
                },
              ],
            },
            allDates: {
              doc_count_error_upper_bound: 0,
              sum_other_doc_count: 0,
              buckets: [
                {
                  key: 1683670977000,
                  key_as_string: '2023-05-09T22:22:57.000Z',
                  doc_count: 3,
                },
                {
                  key: 1683667377000,
                  key_as_string: '2023-05-09T21:22:57.000Z',
                  doc_count: 1,
                },
                {
                  key: 1675718577000,
                  key_as_string: '2023-02-06T21:22:57.000Z',
                  doc_count: 1,
                },
              ],
            },
          },
        } as T);
      }

      return of({} as T);
    }

    elasticsearchService = TestBed.inject(ElasticsearchService);
    templateSearchSpy = spyOn(
      elasticsearchService,
      'templateSearch'
    ).and.callFake(fakeCall);

    service = TestBed.inject(DashboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return last run', (done: DoneFn) => {
    service.getLastRun().subscribe(r => {
      expect(r.success).toBe(24);
      expect(r.error).toBe(2);
      expect(r.ignored).toBe(13);
      expect(r.inconclusive).toBe(2);
      expect(r.failure).toBe(4);
      done();
    });
  });

  it('should return the testenvironment history useable for chart.js', (done: DoneFn) => {
    service.getTestenvironmentHistory().subscribe(r => {
      expect(r.labels.length).toBe(3);
      expect(r.labels[0]).toBe(dateToYYYYMMDDHHMMSS(new Date('2023-05-09T22:22:57.000Z')));
      expect(r.labels[1]).toBe(dateToYYYYMMDDHHMMSS(new Date('2023-05-09T21:22:57.000Z')));
      expect(r.labels[2]).toBe(dateToYYYYMMDDHHMMSS(new Date('2023-02-06T21:22:57.000Z')));

      expect(r.datasets.length).toBe(3);
      expect(r.datasets[0].label).toBe('machine-name-A');
      expect(r.datasets[1].label).toBe('machine-name-B');
      expect(r.datasets[2].label).toBe('machine-name-C');

      expect(r.datasets[0].data).toEqual([0, 0, 0]);
      expect(r.datasets[1].data).toEqual([3, 1, 1]);
      expect(r.datasets[2].data).toEqual([0, 3, 1]);
      done()
    });
  });
});

describe('DashboardService with HTTP Errors', () => {
  let service: DashboardService;
  let elasticsearchService: ElasticsearchService;
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpClientSpy }]
    });

    httpClientSpy.get.and.returnValue(throwError(() => new Error()));
    httpClientSpy.post.and.returnValue(throwError(() => new Error()));
    elasticsearchService = TestBed.inject(ElasticsearchService);
    service = TestBed.inject(DashboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should do nothing when last run is called an an HTTP error occurs', (done: DoneFn) => {
    service.getLastRun()
    .pipe(
      catchError(e => {
        return 'e';
      })
    )
    .subscribe(r => {
      expect(r).toEqual('e')
      done();
    })
  });

  it('should do nothing when testenvironment history is called an an HTTP error occurs', (done: DoneFn) => {
    service.getLastRun()
    .pipe(
      catchError(e => {
        return 'e';
      })
    )
    .subscribe(r => {
      expect(r).toEqual('e')
      done();
    })
  });


});
