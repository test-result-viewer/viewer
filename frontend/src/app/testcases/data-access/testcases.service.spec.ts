import { TestBed } from '@angular/core/testing';

import { TestcasesService } from './testcases.service';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Testresult } from 'src/app/types/testresults.types';

describe('TestresultDetailService', () => {
  let service: TestcasesService;
  let elasticsearchService: ElasticsearchService;
  let templateSearchSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: {} }],
    });
    service = TestBed.inject(TestcasesService);

    elasticsearchService = TestBed.inject(ElasticsearchService);
    templateSearchSpy = spyOn(
      elasticsearchService,
      'templateSearch'
    ).and.returnValue(of(
      {
        "took": 3,
        "timed_out": false,
        "_shards": {
          "total": 1,
          "successful": 1,
          "skipped": 0,
          "failed": 0
        },
        "hits": {
          "total": {
            "value": 986,
            "relation": "eq"
          },
          "max_score": null,
          "hits": [
            {
              "_index": "testresult",
              "_id": "seY8v4gBZm6mj1uD-p5J",
              "_score": null,
              "_source": {
                "version-control-system-info": [
                  {
                    "commit-hash": "c11934f7f1354a31c8130ca5ce159d6ac2697689",
                    "commit-author": "thomas@sonova.com",
                    "commit-message": "code cleanup",
                    "repository": "someotherrepo",
                    "type": "svn",
                    "branch": "dev",
                    "commit-time": "2023-04-19T11:19:09.112064Z"
                  }
                ],
                "reason": "reason for failure",
                "start-time": "2023-05-06T23:59:59.000000Z",
                "testsuite": "suite4",
                "culture-info": {
                  "current-uiculture": "en-US",
                  "current-culture": "de-CH"
                },
                "description": "some description to be randomized",
                "executed": false,
                "testcase": "some test case",
                "testassembly": "aVerySpecial.dll",
                "labels": [],
                "result": "ignored",
                "asserts": 0,
                "environment": {
                  "os-version": "SomeOSVersion",
                  "cwd": "C:\\path\\to\\src",
                  "framework": "nunit",
                  "user-domain": "domain2",
                  "framework-version": "2.3.4.5.6",
                  "clr-version": "3.0.1234.5678",
                  "user": "Andreas",
                  "platform": "UNIX",
                  "machine-name": "this-PC"
                },
                "build": {
                  "identifier": "build id",
                  "number": 222,
                  "project": "project",
                  "trigger": "trigger",
                  "brand": "brand",
                  "url": "url"
                },
                "failure": {
                  "stacktrace": "",
                  "message": ""
                },
                "time": 10.386,
                "categories": [
                  "HealthChecks",
                  "HealthChecks"
                ],
                "test-environment": {
                  "capabilities": [
                    "label #2"
                  ],
                  "name": "host-A"
                },
                "properties": [
                  {
                    "name": "propery1",
                    "value": "123456"
                  }
                ]
              },
              "fields": {
                "testcase.keyword": [
                  "some test case"
                ]
              },
              "sort": [
                "some test case"
              ]
            },
            {
              "_index": "testresult",
              "_id": "j-Y8v4gBZm6mj1uD-p5J",
              "_score": null,
              "_source": {
                "version-control-system-info": [
                  {
                    "commit-hash": "4b6b8b9442bfb9999707b624a83d9168ceed8243",
                    "commit-author": "darek@sonova.com",
                    "commit-message": "code cleanup",
                    "repository": "someotherrepo",
                    "type": "svn",
                    "branch": "branch2",
                    "commit-time": "2023-05-10T20:16:51.774782Z"
                  }
                ],
                "reason": "reason for failure",
                "start-time": "2023-05-14T23:59:59.000000Z",
                "testsuite": "suite4",
                "culture-info": {
                  "current-uiculture": "de-CH",
                  "current-culture": "en-US"
                },
                "description": "some description to be randomized",
                "executed": true,
                "testcase": "test case 1",
                "testassembly": "another.dll",
                "labels": [],
                "result": "success",
                "asserts": 0,
                "environment": {
                  "os-version": "SomeOSVersion",
                  "cwd": "C:\\test\\directory",
                  "framework": "nunit",
                  "user-domain": "COMPANY1",
                  "framework-version": "2.3.4.5.6",
                  "clr-version": "3.0.1234.5678",
                  "user": "Darek",
                  "platform": "Win32NT",
                  "machine-name": "that-PC"
                },
                "build": {
                  "identifier": "build id",
                  "number": 145,
                  "project": "project",
                  "trigger": "trigger",
                  "brand": "brand",
                  "url": "url"
                },
                "failure": {
                  "stacktrace": "",
                  "message": ""
                },
                "time": 2.252,
                "categories": [
                  "HealthChecks",
                  "HealthChecks"
                ],
                "test-environment": {
                  "capabilities": [
                    "hardware #1",
                    "hardware #2",
                    "label #2"
                  ],
                  "name": "host-A"
                },
                "properties": [
                  {
                    "name": "propery1",
                    "value": "someothervalue"
                  },
                  {
                    "name": "propery1",
                    "value": "10"
                  }
                ]
              },
              "fields": {
                "testcase.keyword": [
                  "test case 1"
                ]
              },
              "sort": [
                "test case 1"
              ]
            },
          ]
        }
      }
    ));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a distinct list', () => {
    service.getDistinctListByKeyword<string>('testcase').subscribe(r => {
      expect(r.length).toBe(2);
      expect(r[0]).toBe('some test case')
      expect(r[1]).toBe('test case 1')
    })
  });
});
