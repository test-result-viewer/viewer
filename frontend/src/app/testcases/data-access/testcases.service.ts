import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import {
  Hit,
  SearchResponse,
  SortTerm,
} from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
const INDEX_NAME = 'testresult';

@Injectable({
  providedIn: 'root',
})
export class TestcasesService {
  constructor(private elasticsearch: ElasticsearchService) {}
  getDistinctListByKeyword<T>(
    field: keyof Testresult,
    sortTerm?: SortTerm[]
  ): Observable<T[]> {
    return this.elasticsearch
      .templateSearch<SearchResponse<Testresult>>(INDEX_NAME, 'get-distinct', {
        field: `${field}.keyword`,
        sortTerm,
      })
      .pipe(
        map((response: SearchResponse<Testresult>) => {
          const results: T[] = response.hits.hits.map(
            (hit: Hit<Testresult>) => {
              return hit._source[field] as T;
            }
          );
          return results;
        }),
      );
  }
}
