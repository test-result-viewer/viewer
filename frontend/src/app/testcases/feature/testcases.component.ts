import { Component, OnInit } from '@angular/core';
import { TestcasesService } from '../data-access/testcases.service';
import { Observable, catchError, of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { getHTTPErrorAsString, errorSnackBar } from 'src/app/shared/utilities/utilities';

const DEFAULT_DATA = ['no data'];

@Component({
  selector: 'app-testcases',
  templateUrl: './testcases.component.html',
  styleUrls: ['./testcases.component.scss'],
})
export class TestcasesComponent implements OnInit {
  testcaseNames$: Observable<string[]> = of(DEFAULT_DATA);

  constructor(
    private service: TestcasesService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.testcaseNames$ = this.getTestcaseNames();
  }

  getTestcaseNames(): Observable<string[]> {
    return this.service.getDistinctListByKeyword<string>('testcase', [
      { 'testcase.keyword': { order: 'asc' } },
    ]).pipe(
      catchError(error => {
        errorSnackBar(this.snackBar, getHTTPErrorAsString(error));
        return of(DEFAULT_DATA);
      })
    );
  }
}
