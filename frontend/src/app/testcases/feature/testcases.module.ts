import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestcasesComponent } from './testcases.component';
import { TestcasesRoutingModule } from './testcases-routing.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [TestcasesComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSnackBarModule,
    TestcasesRoutingModule,
  ],
})
export class TestcasesModule {}
