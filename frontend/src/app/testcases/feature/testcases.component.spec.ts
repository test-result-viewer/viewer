import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { of, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { TestcasesService } from '../data-access/testcases.service';
import { TestcasesComponent } from './testcases.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarErrorComponent } from 'src/app/shared/snackbar/snackbar-error/snackbar-error.component';
import { getHTTPErrorAsString } from 'src/app/shared/utilities/utilities';

describe('TestCasesComponent', () => {
  let component: TestcasesComponent;
  let fixture: ComponentFixture<TestcasesComponent>;
  let de: DebugElement;

  let testcasesService: TestcasesService;
  let getDistinctListSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcasesComponent],
      providers: [
        { provide: TestcasesService },
        { provide: HttpClient, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestcasesComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    testcasesService = de.injector.get(TestcasesService);
    getDistinctListSpy = spyOn(
      testcasesService,
      'getDistinctListByKeyword'
    ).and.returnValue(
      of([
        'test case 1',
        'some test case',
        'very long name test case that might actually better be a description because it is so long..',
      ])
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


describe('TestCasesComponent with HTTP error', () => {
  let component: TestcasesComponent;
  let fixture: ComponentFixture<TestcasesComponent>;
  let de: DebugElement;

  let testcasesService: TestcasesService;
  let snackBar: MatSnackBar;
  let httpError: HttpErrorResponse;
  let getDistinctListSpy: jasmine.Spy;
  let snackBarOpen: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcasesComponent],
      providers: [
        { provide: TestcasesService },
        { provide: HttpClient, useValue: {} },
        { provide: MatSnackBar, snackBar },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestcasesComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    httpError = new HttpErrorResponse({status: 404, statusText: 'Not found'})
    snackBar = de.injector.get(MatSnackBar);
    snackBarOpen = spyOn(snackBar, 'openFromComponent');
    testcasesService = de.injector.get(TestcasesService);
    getDistinctListSpy = spyOn(
      testcasesService,
      'getDistinctListByKeyword'
    ).and.returnValue(throwError(() => { return httpError}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open snackbar on error', (done: DoneFn) => {
    snackBarOpen.calls.reset();
    component.testcaseNames$.subscribe(r => {
      expect(snackBarOpen.calls.count()).toBe(1);
      expect(snackBarOpen.calls.first().args).toEqual([SnackbarErrorComponent, { data: getHTTPErrorAsString(httpError), duration: 10_000}] )
      done();
    })

    expect(component).toBeTruthy();
  });
});
