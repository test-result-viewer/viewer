import { Injectable } from '@angular/core';
import { SearchResponse } from 'src/app/types/elasticsearch.types';
import {
  Observable,
  catchError,
  combineLatest,
  map,
  of,
  switchMap,
  tap,
} from 'rxjs';
import {
  Hit,
  Bucket,
  Hits,
  UpdateResponse,
} from 'src/app/types/elasticsearch.types';
import {
  Assignment,
  CommentWithId,
  Filter,
  Testresult,
  VCSInfo,
  Comment,
  AssignmentWithId,
  Testcase,
} from 'src/app/types/testresults.types';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import * as esb from 'elastic-builder';
import { HttpErrorResponse } from '@angular/common/http';

export interface ByCommit {
  key: string;
  results: Hit<Testresult>[];
}

export interface ByTestcaseByCommit {
  name: string;
  comment: CommentWithId | undefined;
  assignment: AssignmentWithId | undefined;
  health: string;
  results: ByCommit[];
}

export interface ByTestsuiteByTestcaseByCommit {
  key: string;
  resultCount?: number;
  health?: string;
  isExpanded: boolean;
  testCase: ByTestcaseByCommit[];
}

export interface TestcaseHistoryData {
  commits: VCSInfo[];
  testSuites: ByTestsuiteByTestcaseByCommit[];
  displayedColumns: string[];
}

export interface TestcaseBucket extends Bucket {
  health: {
    hits: Hits<Testresult>;
  };
  byCommit: {
    doc_count_error_upper_bound: number;
    sum_other_doc_count: number;
    buckets: CommitBucket[];
  };
}

export interface CommitBucket extends Bucket {
  result: {
    hits: Hits<Testresult>;
  };
}

export interface TestsuiteBucket extends Bucket {
  byTestcase: {
    doc_count_error_upper_bound: number;
    sum_other_doc_count: number;
    buckets: TestcaseBucket[];
  };
}

interface BranchNamesQueryResponse extends SearchResponse {
  aggregations: {
    byBranch: {
      buckets: {
        key: string;
        doc_count: number;
      }[];
    };
  };
}

export interface TestsuiteAggregationQueryReponse extends SearchResponse {
  aggregations: {
    byTestsuite: {
      doc_count_error_upper_bound: number;
      sum_other_doc_count: number;
      buckets: {
        key: string;
        doc_count: number;
        byTestcase: {
          doc_count_error_upper_bound: number;
          sum_other_doc_count: number;
          buckets: {
            key: string;
            doc_count: number;
            byCommit: {
              doc_count_error_upper_bound: number;
              sum_other_doc_count: number;
              buckets: {
                key: string;
                doc_count: number;
                result: {
                  hits: {
                    total: {
                      value: number;
                      relation: string;
                    };
                    max_score: number;
                    hits: Hit<Testresult>[];
                  };
                };
              }[];
            };
            health: {
              hits: {
                total: {
                  value: number;
                  relation: string;
                };
                max_score: null;
                hits: Hit<Testresult>[];
              };
            };
          }[];
        };
      }[];
    };
  };
}

export interface LastCommitsQueryResponse extends SearchResponse {
  aggregations: {
    byTime: {
      doc_count_error_upper_bound: number;
      sum_other_doc_count: number;
      buckets: {
        key: number;
        key_as_string: string;
        doc_count: number;
        topCommits: {
          hits: {
            total: {
              value: number;
              relation: string;
            };
            max_score: null;
            hits: Hit<Testresult>[];
          };
        };
      }[];
    };
  };
}

const TESTRESTULT_INDEX_NAME = 'testresult';
const ASSIGNMENT_INDEX_NAME = 'assignment';
const COMMENT_INDEX_NAME = 'comment';

@Injectable({
  providedIn: 'root',
})
export class TestcaseHistoryService {
  constructor(private elasticsearch: ElasticsearchService) {}

  getBranchList(): Observable<string[]> {
    return this.elasticsearch
      .templateSearch<BranchNamesQueryResponse>(
        TESTRESTULT_INDEX_NAME,
        'get-branch-names',
        {}
      )
      .pipe(
        map((response: BranchNamesQueryResponse) =>
          response.aggregations['byBranch'].buckets.map(b => b.key)
        ),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
  }

  getAssignmentList(): Observable<string[]> {
    return this.elasticsearch
      .templateSearch<SearchResponse<Assignment>>(
        ASSIGNMENT_INDEX_NAME,
        'get-distinct',
        {
          field: 'assignment.keyword',
          sortTerm: [{ 'assignment.keyword': { order: 'asc' } }],
        }
      )
      .pipe(
        map(response => response.hits.hits.map(h => h._source.assignment)),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
  }

  getTableData(
    filter$: Observable<Filter>,
    defaultColumns: string[]
  ): Observable<TestcaseHistoryData> {
    return filter$.pipe(
      map(filter => {
        if (filter.branch === '') return filter;
        if (filter.filter === '') {
          filter.filter = `version-control-system-info.branch:"${filter.branch}"`;
        } else {
          filter.filter = [
            filter.filter,
            `version-control-system.branch:"${filter.branch}"`,
          ].join(' AND ');
        }
        return filter;
      }),
      switchMap(filter =>
        combineLatest([
          of(filter),
          this.getCommitInfo(filter.numberOfCommits || 100, filter),
        ])
      ),
      switchMap(([filter, commits]) =>
        combineLatest([
          of(filter),
          this.getTestcaseHistory(
            filter.numberOfCommits || 100,
            commits,
            defaultColumns,
            filter
          ),
        ])
      ),
      switchMap(([filter, response]) =>
        combineLatest([
          of(response),
          of(filter),
          this.getComments(this.getTestcases(response.testSuites)),
          this.getAssignments(this.getTestcases(response.testSuites)),
        ])
      ),
      map(([data, filter, comments, assignments]) => {
        this.updateTestcaseWithCommentAssignment(data, comments, assignments);

        data.testSuites = data.testSuites.filter(suite => {
          suite.testCase = suite.testCase.filter(
            testcase =>
              (testcase.assignment?.assignment === filter.assignment ||
                filter.assignment === '' ||
                filter.assignment === undefined) &&
              (testcase.comment?.comment === filter.commentFilter ||
                filter.commentFilter === '' ||
                filter.commentFilter === undefined)
          );
          return suite.testCase.length > 0;
        });

        return data;
      })
    );
  }

  upsertAssignment(assignment: AssignmentWithId): Observable<UpdateResponse> {
    if (assignment.id === undefined || assignment.id === '') {
      const query = {
        testcase: assignment.testcase,
        testsuite: assignment.testsuite,
        assignment: assignment.assignment,
      };
      return this.elasticsearch.create(ASSIGNMENT_INDEX_NAME, query).pipe(
        catchError((err: HttpErrorResponse) => { throw err.message })
      );
    } else {
      const query = {
        doc: {
          assignment: assignment.assignment,
        },
      };

      return this.elasticsearch.update(
        ASSIGNMENT_INDEX_NAME,
        assignment.id,
        query
      ).pipe(
        catchError((err: HttpErrorResponse) => { throw err.message })
      );
    }
  }

  upsertComment(comment: CommentWithId): Observable<UpdateResponse> {
    if (comment.id === undefined || comment.id === '') {
      const query = {
        testcase: comment.testcase,
        testsuite: comment.testsuite,
        comment: comment.comment,
      };
      return this.elasticsearch.create(COMMENT_INDEX_NAME, query).pipe(
        catchError((err: HttpErrorResponse) => { throw err.message })
      );
    } else {
      const query = {
        doc: {
          comment: comment.comment,
        },
      };
      return this.elasticsearch.update(
        COMMENT_INDEX_NAME,
        comment.id,
        query
      ).pipe(
        catchError((err: HttpErrorResponse) => { throw err.message })
      );
    }
  }


  private getCommitInfo(
    numberOfCommits: number,
    filter: Filter
  ): Observable<VCSInfo[]> {
    const params: { size: number; must: object } = {
      size: numberOfCommits,
      must: [],
    };

    if (filter.filter) {
      params['must'] = [
        {
          query_string: {
            query: filter.filter,
          },
        },
      ];
    }

    return this.elasticsearch
      .templateSearch<LastCommitsQueryResponse>(
        TESTRESTULT_INDEX_NAME,
        'get-commit-history',
        params
      )
      .pipe(
        map(response => this.getDistinctVCSInfos(response)),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
  }

  private getDistinctVCSInfos(response: LastCommitsQueryResponse): VCSInfo[] {
    return this.removeDuplicateCommitHashes(
      response.aggregations.byTime.buckets.map(bucket => {
        return this.getLatestFromVCSInfos(
          bucket.topCommits.hits.hits[0]._source['version-control-system-info'] ? 
          bucket.topCommits.hits.hits[0]._source['version-control-system-info'] :
          []
        );
      })
    );
  }

  private removeDuplicateCommitHashes(commits: VCSInfo[]): VCSInfo[] {
    const unique: { [x: string]: boolean } = {};
    return commits.filter((commit: VCSInfo) => {
      const key = commit['commit-hash'] + commit['repository'];
      return Object.prototype.hasOwnProperty.call(unique, key)
        ? false
        : (unique[key] = true);
    });
  }

  private getLatestFromVCSInfos(infos: VCSInfo[]): VCSInfo {
    const sortedInfos = infos.sort((a: VCSInfo, b: VCSInfo) => {
      return Date.parse(b['commit-time']) - Date.parse(a['commit-time']);
    });
    if (sortedInfos.length < 1) {
      return {
        branch: '',
        'commit-author': '',
        'commit-hash': '',
        'commit-message': '',
        'commit-time': '',
      };
    }
    return sortedInfos[0];
  }

  private getTestcaseHistory(
    numberOfCommits: number,
    commits: VCSInfo[],
    defaultColumns: string[],
    filter: Filter
  ): Observable<TestcaseHistoryData> {
    const commitInformation = commits.map(vcs => {
      return {
        bool: {
          must: [
            {
              term: {
                'version-control-system-info.commit-hash.keyword':
                  vcs['commit-hash'],
              },
            },
            {
              term: {
                'version-control-system-info.repository.keyword':
                  vcs.repository,
              },
            },
          ],
        },
      };
    });

    const params: { should: object[]; must: object[]; size: number } = {
      should: commitInformation,
      must: [],
      size: numberOfCommits,
    };

    if (filter.filter) {
      params['must'] = [
        {
          query_string: {
            query: filter.filter,
          },
        },
      ];
    }

    return this.elasticsearch
      .templateSearch<TestsuiteAggregationQueryReponse>(
        TESTRESTULT_INDEX_NAME,
        'get-testsuite-aggregation',
        params
      )
      .pipe(
        tap(() => {
          commits.forEach(
            c => (c['commit-hash'] = c['commit-hash'].substring(0, 5))
          );
        }),
        map(response => {
          const testSuites: ByTestsuiteByTestcaseByCommit[] = [];
          response.aggregations.byTestsuite.buckets.forEach(res => {
            testSuites.push({
              key: res.key,
              isExpanded: true,
              testCase: this.transformResponseToTestCases(res),
            });
          });
          const displayedColumns = defaultColumns.concat(
            commits.map(v => {
              return `commit-${v['commit-hash']}-${v['repository']}`;
            })
          );
          return { commits, testSuites, displayedColumns };
        }),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
  }

  private transformResponseToTestCases(
    response: TestsuiteBucket
  ): ByTestcaseByCommit[] {
    return response.byTestcase.buckets.map((bucket: TestcaseBucket) => {
      const name = bucket.key;
      const comment = undefined;
      const health = bucket.health.hits.hits[0]._source.result?.toLowerCase();
      const results = bucket.byCommit.buckets.map(
        (innerBucket: CommitBucket) => {
          return {
            key: innerBucket.key,
            results: innerBucket.result.hits.hits,
          };
        }
      );

      return {
        name,
        comment,
        health,
        results,
      };
    }) as ByTestcaseByCommit[];
  }

  private getComments(testcases: Testcase[]): Observable<CommentWithId[]> {
    // TODO: Replace CommentWithId with Hit<Comment>
    return this.elasticsearch
      .templateSearch<SearchResponse<Comment>>(
        COMMENT_INDEX_NAME,
        'get-with-should-filter',
        {
          filterTerm: this.filterTerms(testcases),
        }
      )
      .pipe(
        map(response => {
          return response.hits.hits.map(d => {
            return {
              id: d._id,
              ...d._source,
            };
          });
        }),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
  }

  private getAssignments(
    testcases: Testcase[]
  ): Observable<AssignmentWithId[]> {
    return this.elasticsearch
      .templateSearch<SearchResponse<Assignment>>(
        ASSIGNMENT_INDEX_NAME,
        'get-with-should-filter',
        {
          filterTerm: this.filterTerms(testcases),
        }
      )
      .pipe(
        map(response => {
          return response.hits.hits.map(d => {
            return {
              id: d._id,
              ...d._source,
            };
          });
        }),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
  }

  private filterTerms(testcases: Testcase[]): object {
    return testcases.map(obj => {
      return esb
        .boolQuery()
        .must([
          esb.termQuery('testcase.keyword', obj.testcase),
          esb.termQuery('testsuite.keyword', obj.testsuite),
        ])
        .toJSON();
    });
  }

  private getTestcases(arr: ByTestsuiteByTestcaseByCommit[]): Testcase[] {
    return arr.flatMap(suite => {
      return suite.testCase.map(testcase => {
        return { testcase: testcase.name, testsuite: suite.key };
      });
    });
  }

  private updateTestcaseWithCommentAssignment(
    data: TestcaseHistoryData,
    comments: CommentWithId[],
    assignments: AssignmentWithId[]
  ) {
    data.testSuites.forEach(suite => {
      suite.testCase.forEach(testcase => {
        const comment = comments.find(c => c.testcase === testcase.name && c.testsuite === suite.key);
        const assignment = assignments.find(a => a.testcase === testcase.name && a.testsuite === suite.key);

        testcase.comment = comment ? comment : { id: undefined, testsuite: suite.key, testcase: testcase.name, comment: '' };
        testcase.assignment = assignment ? assignment : { id: undefined, testsuite: suite.key, testcase: testcase.name, assignment: '' };
      });
    });
  }
}
