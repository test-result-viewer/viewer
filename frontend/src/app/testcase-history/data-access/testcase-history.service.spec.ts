import { TestBed } from '@angular/core/testing';

import { TestcaseHistoryService } from './testcase-history.service';
import { HttpClient } from '@angular/common/http';

describe('TestcaseHistoryService', () => {
  let service: TestcaseHistoryService;
  let httpStub: any;

  beforeEach(() => {
    httpStub = {};
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpStub }],
    });
    service = TestBed.inject(TestcaseHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
