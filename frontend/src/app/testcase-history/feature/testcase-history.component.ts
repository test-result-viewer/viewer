import {
  ChangeDetectionStrategy,
  Component,
  OnInit
} from '@angular/core';
import {
  Observable,
  catchError,
  forkJoin,
  map,
  of,
  switchMap,
  take,
} from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';

import {
  AssignmentWithId,
  CommentWithId,
  Filter,
  Testresult,
  VCSInfo,
} from 'src/app/types/testresults.types';
import * as Utilities from 'src/app/shared/utilities/utilities';
import {
  TestcaseHistoryService,
  TestcaseHistoryData,
  ByTestcaseByCommit,
} from '../data-access/testcase-history.service';
import { HttpErrorResponse } from '@angular/common/http';
import { TestcaseHistoryResultListComponent } from '../ui/testcase-history-result-list/testcase-history-result-list.component';
import { Hit } from 'src/app/types/elasticsearch.types';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { errorSnackBar, infoSnackBar } from 'src/app/shared/utilities/utilities';
import { LoadingService } from 'src/app/shared/data-access/loading/loading.service';

const DEFAULT_DATA: TestcaseHistoryData = {
  commits: [],
  testSuites: [],
  displayedColumns: ['testcase', 'health', 'comment', 'assignment'],
};

const DEFAULT_FILTER = {
  filter: '',
  branch: '',
  assignment: '',
  commentFilter: '',
  numberOfCommits: 100,
};

@Component({
  selector: 'app-testcase-history',
  templateUrl: './testcase-history.component.html',
  styleUrls: ['./testcase-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class TestcaseHistoryComponent implements OnInit {
  filter$: Observable<Filter> = of(DEFAULT_FILTER);
  tableData$?: Observable<TestcaseHistoryData> = of(DEFAULT_DATA);
  branchList$: Observable<string[]> = of([]);
  assignmentList$: Observable<string[]> = of([]);
  dialogRef!: MatDialogRef<TestcaseHistoryResultListComponent>;

  constructor(
    private service: TestcaseHistoryService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    this.branchList$ = this.service.getBranchList();
    this.assignmentList$ = this.service.getAssignmentList();

    // The query$ should be created after the branch & assignment list is received
    this.filter$ = forkJoin([this.branchList$, this.assignmentList$]).pipe(
      switchMap(() => this.getFilter()),
      catchError((msg: string) => {
        errorSnackBar(this.snackBar, msg);
        return of(DEFAULT_FILTER);
      })
    );

    this.tableData$ = this.service
      .getTableData(this.filter$, DEFAULT_DATA.displayedColumns)
      .pipe(
        catchError((msg: string) => {
        errorSnackBar(this.snackBar, msg);
          return of(DEFAULT_DATA);
        })
      );
  }



  getFilter(): Observable<Filter> {
    return this.activeRoute.queryParamMap.pipe(
      map(params => {
        const q = params.get('q');
        const query = q == null ? this.defaultQuery : q === '' ? '' : q;
        const branch = params.get('b') || '';
        const assignment = params.get('a') || '';
        const comment = params.get('c') || '';
        const numberOfCommits = Number(params.get('n')) || 0;
        return {
          filter: query,
          branch,
          assignment,
          commentFilter: comment,
          numberOfCommits,
        };
      })
    );
  }

  getSymbol(result: string): string {
    return Utilities.getSymbol(result);
  }

  getResultForCommit(
    testCase: ByTestcaseByCommit,
    commitHash: string
  ): Hit<Testresult>[] | undefined {
    return testCase.results.find(value => {
      return value.key.startsWith(commitHash);
    })?.results;
  }

  getOverallResult(results: Hit<Testresult>[]): string {
    return (
      results.reduce(
        (total: Hit<Testresult>, commitResult: Hit<Testresult>) => {
          return total._source.result?.toLowerCase() === 'success'
            ? commitResult
            : total;
        }
      )._source.result || 'investigated'
    );
  }

  isAllGreen(results: Hit<Testresult>[]): boolean {
    return this.getOverallResult(results).toLocaleLowerCase() === 'success';
  }

  buildTooltipText(hits: Hit<Testresult>[]): string {
    const labels = hits
      .filter(
        h => h._source.labels !== undefined && h._source.labels.length > 0
      )
      .map(h => {
        return (
          'Labels:\n' +
            h._source.labels?.map(l => `${l.key}: ${l.value}`).join('\n') || ''
        );
      })
      .join('\n');

    const errors = hits
      .filter(h => h._source.result !== 'success')
      .map(
        h => `${h._source.failure?.message}\n${h._source.failure?.stacktrace}`
      )
      .join('\n\n');

    return `# of Jobs: ${hits.length}\n\n`.concat(labels, errors);
  }

  buildCommitTooltipText(commit: VCSInfo): string {
    return [
      `Commit Time: ${Utilities.dateToYYYYMMDDHHMMSS(
        new Date(commit['commit-time'])
      )}`,
      `Commit Repo: ${commit.repository}`,
      `Commit Branch: ${commit.branch}`,
      `Commit Hash: ${commit['commit-hash']}`,
      `Commit Author: ${commit['commit-author']}`,
      `Commit Message: ${commit['commit-message']}`,
    ].join('\n');
  }

  onResultClick(hits: Hit<Testresult>[]) {
    this.dialogRef = this.dialog.open(TestcaseHistoryResultListComponent, {
      data: hits,
    });
  }

  onFilter(filter: Filter): void {
    const queryParams: Params = {
      q: filter.filter,
      b: filter.branch,
      a: filter.assignment,
      c: filter.commentFilter,
      n: filter.numberOfCommits,
    };

    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParamsHandling: 'merge',
      queryParams,
    });
  }

  onSaveAssignment(assignment: AssignmentWithId): void {
    this.service.upsertAssignment(assignment)
      .pipe(
        take(1), // Subscription should only act on 1 event, then be removed again.
        catchError(response => {
          if (response instanceof HttpErrorResponse) {
            errorSnackBar(this.snackBar, JSON.stringify(response))
          }
          return of(response);
        })
      )
      .subscribe({
        next: response => {
          if (response.result == 'updated' || response.result == 'created') {
            infoSnackBar(this.snackBar, 'Assignment updated');
          } else {
            errorSnackBar(this.snackBar, JSON.stringify(response));
          }
        },
      });
  }

  onSaveComment(comment: CommentWithId) {
    this.service.upsertComment(comment)
      .pipe(
        take(1), // Subscription should only act on 1 event, then be removed again.
        catchError(response => {
          if (response instanceof HttpErrorResponse) {
            errorSnackBar(this.snackBar, JSON.stringify(response));
          }
          return of(response);
        }),
      )
      .subscribe(response => {
        if (response.result == 'updated' || response.result == 'created') {
          infoSnackBar(this.snackBar, 'Comment updated');
        } else {
          errorSnackBar(this.snackBar, JSON.stringify(response));
        }
      });
  }

  private defaultQuery = 'start-time:[now-7d/d TO *]';
}
