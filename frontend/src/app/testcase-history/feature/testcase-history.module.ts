import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TableVirtualScrollModule } from 'ng-table-virtual-scroll';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { TestcaseHistoryRoutingModule } from './testcase-history-routing.module';
import { TestcaseHistoryLegendModule } from '../ui/testcase-history-legend/testcase-history-legend.module';
import { TestcaseHistoryResultListModule } from '../ui/testcase-history-result-list/testcase-history-result-list.module';
import { TestcaseHistoryComponent } from './testcase-history.component';
import { TestcaseHistoryAssignmentModule } from '../ui/testcase-history-assignment/testcase-history-assignment.module';
import { TestcaseHistoryCommentModule } from '../ui/testcase-history-comment/testcase-history-comment.module';
import { TestcaseHistoryFilterModule } from '../ui/testcase-history-filter/testcase-history-filter.module';
import { TestcaseHistoryAssignmentDialogModule } from '../ui/testcase-history-assignment-dialog/testcase-history-assignment-dialog.module';
import { TestcaseHistoryCommentDialogModule } from '../ui/testcase-history-comment-dialog/testcase-history-comment-dialog.module';

@NgModule({
  declarations: [TestcaseHistoryComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    ScrollingModule,
    MatTooltipModule,
    TableVirtualScrollModule,
    MatDialogModule,
    MatSnackBarModule,
    TestcaseHistoryLegendModule,
    TestcaseHistoryCommentModule,
    TestcaseHistoryAssignmentModule,
    TestcaseHistoryResultListModule,
    TestcaseHistoryFilterModule,
    TestcaseHistoryRoutingModule,
    TestcaseHistoryAssignmentDialogModule,
    TestcaseHistoryCommentDialogModule
  ],
  exports: [TestcaseHistoryComponent],
})
export class TestcaseHistoryModule {}
