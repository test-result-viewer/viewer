export const getCommitInfo = [
  {
    branch: 'test',
    repo: 'someotherrepo',
    'commit-hash': 'd9c71',
    'commit-time': '2023-04-21T09:39:42.172038Z',
    'commit-author': 'someone@sonova.com',
    'commit-message': 'bugfix',
  },
  {
    branch: 'test',
    repo: 'repo4',
    'commit-hash': 'ed0c9',
    'commit-time': '2023-04-20T09:37:38.716682Z',
    'commit-author': 'someone@sonova.com',
    'commit-message': 'bugfix',
  },
  {
    branch: 'test',
    repo: 'somerepo',
    'commit-hash': '115a8',
    'commit-time': '2023-04-18T22:01:06.228952Z',
    'commit-author': 'someone@sonova.com',
    'commit-message': 'bugfix',
  },
  {
    branch: 'prod',
    repo: 'reponame',
    'commit-hash': '3e86d',
    'commit-time': '2023-04-16T03:02:02.742146Z',
    'commit-author': 'someone@sonova.com',
    'commit-message': 'bugfix',
  },
  {
    branch: 'prod',
    repo: 'reponame',
    'commit-hash': '4fb46',
    'commit-time': '2023-04-15T15:33:48.733192Z',
    'commit-author': 'someone@sonova.com',
    'commit-message': 'bugfix',
  },
];

export const getDistinctBranchNames = ['dev', 'prod', 'test'];

export const getSymbol = 'x';

export const tableData = {
  commits: [
    {
      branch: '',
      repo: '',
      'commit-hash': '',
      'commit-time': '',
      'commit-author': '',
      'commit-message': '',
    },
    {
      branch: 'test',
      repo: 'repo4',
      'commit-hash': 'ed0c9',
      'commit-time': '2023-04-20T09:37:38.716682Z',
      'commit-author': 'someone@sonova.com',
      'commit-message': 'bugfix',
    },
    {
      branch: 'test',
      repo: 'somerepo',
      'commit-hash': '115a8',
      'commit-time': '2023-04-18T22:01:06.228952Z',
      'commit-author': 'someone@sonova.com',
      'commit-message': 'bugfix',
    },
    {
      branch: 'prod',
      repo: 'reponame',
      'commit-hash': '3e86d',
      'commit-time': '2023-04-16T03:02:02.742146Z',
      'commit-author': 'someone@sonova.com',
      'commit-message': 'bugfix',
    },
    {
      branch: 'prod',
      repo: 'reponame',
      'commit-hash': '4fb46',
      'commit-time': '2023-04-15T15:33:48.733192Z',
      'commit-author': 'someone@sonova.com',
      'commit-message': 'bugfix',
    },
  ],
  testSuites: [
    {
      key: 'suite1',
      isExpanded: false,
      testCase: [
        {
          name: 'test case 5',
          comment: undefined,
          assignment: undefined,
          health: 'success',
          results: [
            {
              key: '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
              results: [
                {
                  _id: 'EZmXs4cB4ApC2rW2pmOB',
                  _index: 'testresult',
                  _score: 1,
                  _source: {
                    testsuite: 'suite1',
                    'start-time': '2023-02-12T20:12:32.000000Z',
                    build: {
                      identifier: 'build id',
                      brand: '',
                      number: 0,
                      project: '',
                      trigger: 'commit',
                      url: '',
                    },
                    'version-control-system-info': [
                      {
                        branch: 'test',
                        'commit-hash':
                          'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
                        'commit-time': '2023-04-21T09:39:42.172038Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                      {
                        branch: 'dev',
                        'commit-hash':
                          '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
                        'commit-time': '2023-02-15T13:55:12.585323Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                    ],
                    executed: true,
                    result: 'success',
                  },
                },
              ],
            },
            {
              key: 'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
              results: [
                {
                  _id: 'EZmXs4cB4ApC2rW2pmOB',
                  _index: 'testresult',
                  _score: 1,
                  _source: {
                    testsuite: 'suite1',
                    'start-time': '2023-02-12T20:12:32.000000Z',
                    build: {
                      identifier: 'build id',
                      brand: '',
                      number: 0,
                      project: '',
                      trigger: 'commit',
                      url: '',
                    },
                    'version-control-system-info': [
                      {
                        branch: 'test',
                        'commit-hash':
                          'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
                        'commit-time': '2023-04-21T09:39:42.172038Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                      {
                        branch: 'dev',
                        'commit-hash':
                          '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
                        'commit-time': '2023-02-15T13:55:12.585323Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                    ],
                    executed: true,
                    result: 'success',
                  },
                },
              ],
            },
          ],
        },
      ],
    },
    {
      key: 'suite2',
      isExpanded: false,
      testCase: [
        {
          name: 'test case 29',
          comment: undefined,
          assignment: undefined,
          health: 'error',
          results: [
            {
              key: '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
              results: [
                {
                  _id: 'GZmXs4cB4ApC2rW2pmOB',
                  _index: 'testresult',
                  _score: 1,
                  _source: {
                    testsuite: 'suite2',
                    'start-time': '2023-02-03T20:12:32.000000Z',
                    build: {
                      identifier: 'build id',
                      brand: '',
                      number: 0,
                      project: '',
                      trigger: 'commit',
                      url: '',
                    },
                    'version-control-system-info': [
                      {
                        branch: 'test',
                        'commit-hash':
                          'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
                        'commit-time': '2023-04-21T09:39:42.172038Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                      {
                        branch: 'dev',
                        'commit-hash':
                          '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
                        'commit-time': '2023-02-15T13:55:12.585323Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                    ],
                    executed: true,
                    result: 'error',
                  },
                },
                {
                  _id: 'GpmXs4cB4ApC2rW2pmOB',
                  _index: 'testresult',
                  _score: 1,
                  _source: {
                    testsuite: 'suite2',
                    'start-time': '2023-02-27T20:12:32.000000Z',
                    build: {
                      identifier: 'build id',
                      brand: '',
                      number: 0,
                      project: '',
                      trigger: 'commit',
                      url: '',
                    },
                    'version-control-system-info': [
                      {
                        branch: 'test',
                        'commit-hash':
                          'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
                        'commit-time': '2023-04-21T09:39:42.172038Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                      {
                        branch: 'dev',
                        'commit-hash':
                          '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
                        'commit-time': '2023-02-15T13:55:12.585323Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                    ],
                    executed: true,
                    result: 'ignored',
                  },
                },
              ],
            },
            {
              key: 'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
              results: [
                {
                  _id: 'GZmXs4cB4ApC2rW2pmOB',
                  _index: 'testresult',
                  _score: 1,
                  _source: {
                    testsuite: 'suite2',
                    'start-time': '2023-02-03T20:12:32.000000Z',
                    build: {
                      identifier: 'build id',
                      brand: '',
                      number: 0,
                      project: '',
                      trigger: 'commit',
                      url: '',
                    },
                    'version-control-system-info': [
                      {
                        branch: 'test',
                        'commit-hash':
                          'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
                        'commit-time': '2023-04-21T09:39:42.172038Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                      {
                        branch: 'dev',
                        'commit-hash':
                          '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
                        'commit-time': '2023-02-15T13:55:12.585323Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                    ],
                    executed: true,
                    result: 'error',
                  },
                },
                {
                  _id: 'GpmXs4cB4ApC2rW2pmOB',
                  _index: 'testresult',
                  _score: 1,
                  _source: {
                    testsuite: 'suite2',
                    'start-time': '2023-02-27T20:12:32.000000Z',
                    build: {
                      identifier: 'build id',
                      brand: '',
                      number: 0,
                      project: '',
                      trigger: 'commit',
                      url: '',
                    },
                    'version-control-system-info': [
                      {
                        branch: 'test',
                        'commit-hash':
                          'd9c7160bbb5b28f1149457cdc9ca43fc0238fb2d',
                        'commit-time': '2023-04-21T09:39:42.172038Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                      {
                        branch: 'dev',
                        'commit-hash':
                          '1c4d5cb5695403fd031698b5d311c8d01bdfae07',
                        'commit-time': '2023-02-15T13:55:12.585323Z',
                        'commit-author': 'someone@sonova.com',
                        'commit-message': 'bugfix',
                      },
                    ],
                    executed: true,
                    result: 'ignored',
                  },
                },
              ],
            },
          ],
        },
      ],
    },
  ],
  displayedColumns: ['testcase', 'health', 'comment', 'assignment'],
};

export const branchList = ['dev', 'prod', 'bug-123'];

export const assignmentList = ['thomas', 'darek'];
