import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestcaseHistoryComponent } from './testcase-history.component';
import { TestcaseHistoryService } from '../data-access/testcase-history.service';
import { of } from 'rxjs';
import {
  branchList,
  assignmentList,
  tableData,
} from './testcase-history.component.spec.data';

import { HttpClient } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

describe('TestcaseHistoryComponent', () => {
  let component: TestcaseHistoryComponent;
  let fixture: ComponentFixture<TestcaseHistoryComponent>;

  let testcaseHistoryService: TestcaseHistoryService;
  let getBranchListSpy: jasmine.Spy;
  let getAssignmentListSpy: jasmine.Spy;
  let getTableDataSpy: jasmine.Spy;
  let upsertAssignmentSpy: jasmine.Spy
  let upsertCommentSpy: jasmine.Spy;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcaseHistoryComponent],
      providers: [
        { provide: TestcaseHistoryService },
        { provide: HttpClient, useValue: {} },
        { provide: MatDialog, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
      ],
      imports: [RouterTestingModule.withRoutes([])],
    }).compileComponents();

    fixture = TestBed.createComponent(TestcaseHistoryComponent);
    component = fixture.componentInstance;

    testcaseHistoryService = fixture.debugElement.injector.get(
      TestcaseHistoryService
    );

    getBranchListSpy = spyOn(
      testcaseHistoryService,
      'getBranchList'
    ).and.returnValue(of(branchList));
    getAssignmentListSpy = spyOn(
      testcaseHistoryService,
      'getAssignmentList'
    ).and.returnValue(of(assignmentList));

    getTableDataSpy = spyOn(
      testcaseHistoryService,
      'getTableData'
    ).and.returnValue(of(tableData));

    upsertAssignmentSpy = spyOn(
      testcaseHistoryService,
      'upsertAssignment'
    ).and.returnValue(of());

    upsertCommentSpy = spyOn(
      testcaseHistoryService,
      'upsertComment'
    ).and.returnValue(of());

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create with 3 requests', () => {
    expect(getTableDataSpy.calls.count()).toBe(1);
    expect(getBranchListSpy.calls.count()).toBe(1);
    expect(getAssignmentListSpy.calls.count()).toBe(1);
  });

  it('should have default branch list', (done: DoneFn) => {
    component.branchList$.subscribe(b => {
      expect(b).toContain('dev');
      expect(b).toContain('prod');
      expect(b).toContain('bug-123');
      done();
    });
  });

  it('should have default assignment list', (done: DoneFn) => {
    component.assignmentList$.subscribe(a => {
      expect(a).toContain('thomas');
      expect(a).toContain('darek');
      done();
    });
  });

  it('should have displayed columns with correct values', (done: DoneFn) => {
    component.tableData$!.subscribe(r => {
      // expect(commitInfoSpy.calls.count()).toBeGreaterThanOrEqual(1);
      // expect(snackbarSpy.calls.count()).toEqual(0);
      expect(r.displayedColumns).toContain('testcase');
      expect(r.displayedColumns).toContain('health');
      expect(r.displayedColumns).toContain('comment');
      expect(r.displayedColumns).toContain('assignment');
      done();
    });
  });
});
