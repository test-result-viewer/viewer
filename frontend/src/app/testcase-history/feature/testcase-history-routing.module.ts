import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestcaseHistoryComponent } from './testcase-history.component';

const routes: Routes = [
  {
    path: '',
    component: TestcaseHistoryComponent,
    title: 'Testcase History - Test Result Viewer',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestcaseHistoryRoutingModule {}
