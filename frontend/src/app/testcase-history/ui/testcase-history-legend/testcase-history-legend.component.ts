import { Component } from '@angular/core';
import * as Utilities from 'src/app/shared/utilities/utilities';

@Component({
  selector: 'app-testcase-history-legend',
  templateUrl: './testcase-history-legend.component.html',
  styleUrls: ['./testcase-history-legend.component.scss'],
})
export class TestcaseHistoryLegendComponent {
  getSymbol(result: string): string {
    return Utilities.getSymbol(result);
  }
}
