import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestcaseHistoryLegendComponent } from './testcase-history-legend.component';

@NgModule({
  declarations: [TestcaseHistoryLegendComponent],
  imports: [CommonModule],
  exports: [TestcaseHistoryLegendComponent],
})
export class TestcaseHistoryLegendModule {}
