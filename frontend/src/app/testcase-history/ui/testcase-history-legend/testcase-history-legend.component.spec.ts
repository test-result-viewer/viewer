import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcaseHistoryLegendComponent } from './testcase-history-legend.component';

describe('TestcaseHistoryLegendComponent', () => {
  let component: TestcaseHistoryLegendComponent;
  let fixture: ComponentFixture<TestcaseHistoryLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcaseHistoryLegendComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestcaseHistoryLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
