import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcaseHistoryCommentComponent } from './testcase-history-comment.component';
import { MatDialog } from '@angular/material/dialog';

describe('TestcaseHistoryCommentComponent', () => {
  let component: TestcaseHistoryCommentComponent;
  let fixture: ComponentFixture<TestcaseHistoryCommentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcaseHistoryCommentComponent],
      providers: [
        { provide: MatDialog, useValue: {} }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TestcaseHistoryCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
