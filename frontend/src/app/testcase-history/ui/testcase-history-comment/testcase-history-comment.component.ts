import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CommentWithId } from 'src/app/types/testresults.types';
import { TestcaseHistoryCommentDialogComponent } from '../testcase-history-comment-dialog/testcase-history-comment-dialog.component';

@Component({
  selector: 'app-testcase-history-comment',
  templateUrl: './testcase-history-comment.component.html',
  styleUrls: ['./testcase-history-comment.component.scss'],
})
export class TestcaseHistoryCommentComponent {
  @Input()
  set comment(comment: CommentWithId) {
    this._comment = comment;
  }
  get comment() {
    return this._comment;
  }

  @Output() commentUpdated = new EventEmitter<CommentWithId>();
  dialogRef: MatDialogRef<TestcaseHistoryCommentDialogComponent> | undefined;

  constructor(
    private dialog: MatDialog
  ) {}

  editComment(): void {
    this.dialogRef = this.dialog.open(
      TestcaseHistoryCommentDialogComponent,
      {
        data: this._comment.comment,
        autoFocus: true
      }
    )

    this.dialogRef.afterClosed().subscribe(result => {
      if(this.comment.comment != result) {
        this.comment.comment = result;
        this.commentUpdated.emit(this.comment);
      }
    })
  }

  private _comment: CommentWithId = {
    testcase: '',
    testsuite: '',
    comment: '',
    id: undefined,
  };
}
