import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs';

@Component({
  selector: 'app-testcase-history-comment-dialog',
  templateUrl: './testcase-history-comment-dialog.component.html',
  styleUrls: ['./testcase-history-comment-dialog.component.scss']
})
export class TestcaseHistoryCommentDialogComponent implements OnInit {
  commentForm: FormGroup = new FormGroup([]);

  constructor(
    public dialogRef: MatDialogRef<TestcaseHistoryCommentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public comment: string
  ) {
    this.commentForm = new FormGroup({
      comment: new FormControl(this.comment)
    })
  }

  ngOnInit(): void {
    this.dialogRef.keydownEvents().pipe(take(1)).subscribe(event => {
      if (event.key === "Escape") {
        this.onCancel();
      }
    });

    this.dialogRef.backdropClick().pipe(take(1)).subscribe(() => {
        this.onCancel();
    });
  }

  get commentControl() {
    return this.commentForm.get('comment');
  }

  onSave() {
    this.dialogRef.close(this.commentControl?.value.trim());
  }

  onCancel() {
    this.dialogRef.close(this.comment);
  }
}
