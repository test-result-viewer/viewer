import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestcaseHistoryCommentDialogComponent } from './testcase-history-comment-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
  declarations: [
    TestcaseHistoryCommentDialogComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule
  ],
  exports: [TestcaseHistoryCommentDialogComponent]
})
export class TestcaseHistoryCommentDialogModule { }
