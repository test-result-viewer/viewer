import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { AssignmentWithId } from 'src/app/types/testresults.types';
import { TestcaseHistoryAssignmentDialogComponent } from '../testcase-history-assignment-dialog/testcase-history-assignment-dialog.component';
import { LogService } from 'src/app/shared/data-access/logs/log.service';

@Component({
  selector: 'app-testcase-history-assignment',
  templateUrl: './testcase-history-assignment.component.html',
  styleUrls: ['./testcase-history-assignment.component.scss'],
})
export class TestcaseHistoryAssignmentComponent {
  @Input()
  set assignment(assignment: AssignmentWithId) {
    this._assignment = assignment;
  }
  get assignment() {
    return this._assignment;
  }

  @Output() assignmentUpdated = new EventEmitter<AssignmentWithId>();
  dialogRef: MatDialogRef<TestcaseHistoryAssignmentDialogComponent> | undefined;

  constructor(
    private dialog: MatDialog,
    private logger: LogService
  ) {}

  editAssignment(config?: MatDialogConfig): void {
    this.dialogRef = this.dialog.open(
      TestcaseHistoryAssignmentDialogComponent,
      {
        ...config,
        data: this._assignment.assignment,
        autoFocus: true
      }
    )
    this.dialogRef.afterClosed().subscribe(result => {
      this.logger.debug(result);
      if (this._assignment.assignment != result) {
        this._assignment.assignment = result;
        this.assignmentUpdated.emit(this._assignment);
      }
    })
  }

  private _assignment: AssignmentWithId = {
    testcase: '',
    testsuite: '',
    assignment: '',
    id: undefined,
  };
}
