import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestcaseHistoryAssignmentComponent } from './testcase-history-assignment.component';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogHarness } from '@angular/material/dialog/testing'
import { HarnessLoader } from '@angular/cdk/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';

describe('TestcaseHistoryAssignmentComponent', () => {
  let component: TestcaseHistoryAssignmentComponent;
  let fixture: ComponentFixture<TestcaseHistoryAssignmentComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcaseHistoryAssignmentComponent],
      providers: [
        { provide: MatDialog, useValue: {} }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TestcaseHistoryAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should load harness for dialog', async () => {
  //   component.editAssignment();
  //   const dialogs = await loader.getAllHarnesses(MatDialogHarness);
  //   expect(dialogs.length).toBe(1);
  // });
});
