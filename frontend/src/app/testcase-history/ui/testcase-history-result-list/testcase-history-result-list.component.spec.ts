import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcaseHistoryResultListComponent } from './testcase-history-result-list.component';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';

describe('TestcaseHistoryResultListComponent', () => {
  let component: TestcaseHistoryResultListComponent;
  let fixture: ComponentFixture<TestcaseHistoryResultListComponent>;

  let model: Hit<Testresult>[] = [
    {
      _id: '1',
      _index: 'testresult',
      _score: 1,
      _source: {} as Testresult,
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [TestcaseHistoryResultListComponent],
      providers: [
        { provide: MatDialog, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: model },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestcaseHistoryResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
