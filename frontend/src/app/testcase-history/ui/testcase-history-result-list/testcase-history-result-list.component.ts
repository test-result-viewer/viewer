import { Component, Inject } from '@angular/core';
import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-testcase-history-result-list',
  templateUrl: './testcase-history-result-list.component.html',
  styleUrls: ['./testcase-history-result-list.component.scss'],
})
export class TestcaseHistoryResultListComponent {
  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<TestcaseHistoryResultListComponent>,
    @Inject(MAT_DIALOG_DATA) public resultHits: Hit<Testresult>[]
  ) {}

  navigateTo(id: string) {
    this.dialogRef.close();
    this.router.navigate(['/testresult-detail'], { queryParams: { id } });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
