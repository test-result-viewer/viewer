import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { TestcaseHistoryResultListComponent } from './testcase-history-result-list.component';
import {
  MAT_DIALOG_DATA,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogModule,
} from '@angular/material/dialog';

@NgModule({
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
      },
    },
  ],
  declarations: [TestcaseHistoryResultListComponent],
  imports: [CommonModule, MatButtonModule, MatDialogModule],
  exports: [TestcaseHistoryResultListComponent],
})
export class TestcaseHistoryResultListModule {}
