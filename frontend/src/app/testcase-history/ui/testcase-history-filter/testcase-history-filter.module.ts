import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatOptionModule } from '@angular/material/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { TestcaseHistoryFilterComponent } from './testcase-history-filter.component';

@NgModule({
  declarations: [TestcaseHistoryFilterComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatOptionModule,
    FontAwesomeModule,
    MatFormFieldModule,
    ReactiveFormsModule,
  ],
  exports: [TestcaseHistoryFilterComponent],
})
export class TestcaseHistoryFilterModule {}
