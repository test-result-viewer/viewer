import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { faShareFromSquare } from '@fortawesome/free-regular-svg-icons';
import { Filter } from 'src/app/types/testresults.types';

const QUERY_FILTER_FIELD = 'queryFilter';
const BRANCH_FILTER_FIELD = 'branchFilter';
const NUMBER_OF_COMMITS_FIELD = 'numberOfCommits';
const ASSIGNMENT_FILTER_FIELD = 'assignmentFilter';
const COMMENT_FILTER_FIELD = 'commentFilter';

@Component({
  selector: 'app-testcase-history-filter',
  templateUrl: './testcase-history-filter.component.html',
  styleUrls: ['./testcase-history-filter.component.scss'],
})
export class TestcaseHistoryFilterComponent {
  @Input()
  set queryFilter(filter: string) {
    this.filterForm.get(QUERY_FILTER_FIELD)?.setValue(filter);
  }
  get queryFilter() {
    return this.filterForm.get(QUERY_FILTER_FIELD)?.value;
  }

  @Input()
  set branchFilter(branch: string) {
    this.filterForm.get(BRANCH_FILTER_FIELD)?.setValue(branch);
  }
  get branchFilter() {
    return this.filterForm.get(BRANCH_FILTER_FIELD)?.value;
  }

  @Input()
  set numberOfCommits(size: number) {
    this.filterForm.get(NUMBER_OF_COMMITS_FIELD)?.setValue(size);
  }
  get numberOfCommits() {
    return this.filterForm.get(NUMBER_OF_COMMITS_FIELD)?.value;
  }

  @Input()
  set assignmentFilter(assignment: string) {
    this.filterForm.get(ASSIGNMENT_FILTER_FIELD)?.setValue(assignment);
  }
  get assignmentFilter() {
    return this.filterForm.get(ASSIGNMENT_FILTER_FIELD)?.value;
  }

  @Input()
  set commentFilter(comment: string) {
    this.filterForm.get(COMMENT_FILTER_FIELD)?.setValue(comment);
  }
  get commentFilter() {
    return this.filterForm.get(COMMENT_FILTER_FIELD)?.value;
  }

  @Input() branchList: string[] = [];
  @Input() assignmentList: string[] = [];

  @Output() filterEvent = new EventEmitter<Filter>();

  faShareFromSquare = faShareFromSquare;
  filterForm: FormGroup = new FormGroup([]);

  constructor() {
    this.filterForm = new FormGroup({
      [QUERY_FILTER_FIELD]: new FormControl(''),
      [BRANCH_FILTER_FIELD]: new FormControl(''),
      [NUMBER_OF_COMMITS_FIELD]: new FormControl(100),
      [ASSIGNMENT_FILTER_FIELD]: new FormControl(''),
      [COMMENT_FILTER_FIELD]: new FormControl(''),
    });
  }

  onFilter() {
    this.filterEvent.emit(this.createFilter());
  }

  lastTwentyFourHours() {
    const query = 'start-time:[now-1d/d TO *]';
    this.queryFilter = query;
    this.filterEvent.emit(this.createFilter());
  }

  lastSevenDays() {
    const query = 'start-time:[now-7d/d TO *]';
    this.queryFilter = query;
    this.filterEvent.emit(this.createFilter());
  }

  clearQuery() {
    this.queryFilter = '';
    this.branchFilter = '';
    this.assignmentFilter = '';
    this.commentFilter = '';
    this.filterEvent.emit(this.createFilter());
  }

  private createFilter(): Filter {
    return {
      filter: this.queryFilter,
      commentFilter: this.commentFilter,
      branch: this.branchFilter,
      numberOfCommits: this.numberOfCommits,
      assignment: this.assignmentFilter,
    };
  }
}
