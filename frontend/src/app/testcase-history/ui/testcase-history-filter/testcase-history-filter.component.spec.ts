import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcaseHistoryFilterComponent } from './testcase-history-filter.component';

describe('TestcaseHistoryFilterComponent', () => {
  let component: TestcaseHistoryFilterComponent;
  let fixture: ComponentFixture<TestcaseHistoryFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestcaseHistoryFilterComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestcaseHistoryFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
