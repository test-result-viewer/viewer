import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs';

@Component({
  selector: 'app-testcase-history-assignment-dialog',
  templateUrl: './testcase-history-assignment-dialog.component.html',
  styleUrls: ['./testcase-history-assignment-dialog.component.scss']
})
export class TestcaseHistoryAssignmentDialogComponent implements OnInit {
  assignmentForm: FormGroup = new FormGroup([]);

  constructor(
    public dialogRef: MatDialogRef<TestcaseHistoryAssignmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public assignment: string
  ) {
    this.assignmentForm = new FormGroup({
      assignment: new FormControl(this.assignment)
    })
  }

  ngOnInit(): void {
    this.dialogRef.keydownEvents().pipe(take(1)).subscribe(event => {
      if (event.key === "Escape") {
        this.onCancel();
      }
    });

    this.dialogRef.backdropClick().pipe(take(1)).subscribe(() => {
      this.onCancel()
    })
  }

  get assignmentControl() {
    return this.assignmentForm.get('assignment');
  }

  onSave() {
    this.dialogRef.close(this.assignmentControl?.value.trim());
  }

  onCancel() {
    this.dialogRef.close(this.assignment);
  }
}
