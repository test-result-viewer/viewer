import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router

export const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/feature/dashboard.module').then(
        m => m.DashboardModule
      ),
  },
  {
    path: 'testcases',
    loadChildren: () =>
      import('./testcases/feature/testcases.module').then(
        m => m.TestcasesModule
      ),
  },
  {
    path: 'testcase-history',
    loadChildren: () =>
      import('./testcase-history/feature/testcase-history.module').then(
        m => m.TestcaseHistoryModule
      ),
  },
  {
    path: 'test-environment',
    loadChildren: () =>
      import('./test-environment/feature/test-environment.module').then(
        m => m.TestEnvironmentModule
      ),
  },
  {
    path: 'testresult-detail',
    loadChildren: () =>
      import('./testresult-detail/feature/testresult-detail.module').then(
        m => m.TestresultDetailModule
      ),
  },
  {
    path: 'regression',
    loadChildren: () =>
      import('./regression/feature/regression.module').then(
        m => m.RegressionModule
      ),
  },
  {
    path: 'database-schema',
    loadChildren: () =>
      import('./database-schema/feature/database-schema.module').then(
        m => m.DatabaseSchemaModule
      ),
    title: 'Database Schema',
  },
  {
    path: '**',
    redirectTo: '/dashboard',
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
