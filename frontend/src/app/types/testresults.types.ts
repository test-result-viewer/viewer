export interface Comment {
  testcase: string;
  testsuite: string;
  comment: string;
}

export interface Assignment {
  testcase: string;
  testsuite: string;
  assignment: string;
}

export interface CommentWithId extends Comment {
  id: string | undefined;
}

export interface AssignmentWithId extends Assignment {
  id: string | undefined;
}

export interface Testcase {
  testcase: string;
  testsuite: string;
}

export interface Build {
  identifier: string;
  version?: number;
  brand?: string;
  number: number;
  project: string;
  trigger: string;
  url: string;
}

export interface VCSInfo {
  branch: string;
  'commit-hash': string;
  'commit-time': string;
  'commit-author': string;
  'commit-message': string;
  repository?: string;
  type?: string;
  column?: string;
}

export interface TestEnvironment {
  name: string;
  capabilities: string[];
}

export interface Environment {
  framework: string;
  'framework-version': string;
  'clr-version': string;
  'os-version': string;
  platform: string;
  cwd: string;
  'machine-name': string;
  user: string;
  'user-domain': string;
}

export interface CultureInfo {
  'current-culture': string;
  'current-uiculture': string;
}

export interface Reason {
  reason: string;
}

export interface Property {
  name: string;
  value: string;
}

export interface Label {
  key: string;
  value: string;
}

export const EMPTY_LABEL = {
  key: '',
  value: '',
} as Label;

export interface Failure {
  message: string;
  stacktrace: string;
}

export interface Testresult {
  id?: string;
  testcase?: string;
  testsuite?: string;
  testassembly?: string;
  description?: string;
  'start-time'?: string;
  build?: Build;
  'version-control-system-info'?: VCSInfo[];
  'test-environment'?: TestEnvironment;
  environment?: Environment;
  'culture-info'?: CultureInfo;
  executed?: boolean;
  result?: string;
  time?: number;
  asserts?: number;
  categories?: string[];
  properties?: Property[];
  labels?: Label[];
  reason?: string;
  failure?: Failure;
}

export interface RegressionFailed {
  key: string;
  runs: Testresult[];
}
export interface Regression {
  key: string;
  runs: Testresult[];
}

export interface Filter {
  branch: string;
  filter: string;
  assignment: string;
  commentFilter: string;
  numberOfCommits: number;
}
