import { Testresult } from './testresults.types';

export interface ShardInfo {
  total: number;
  successful: number;
  skipped: number;
  failed: number;
}

export interface Hits<TDocument> {
  max_score: number | null;
  hits: Hit<TDocument>[];
}

export interface Hit<TDocument> {
  _index: string;
  _id: string;
  _score: string | number | null;
  fields?: {
    [x: string]: string[];
  };
  sort?: number[];
  _source: TDocument;
}

export interface SearchResponse<TDocument = Testresult> {
  took: number;
  timed_out: boolean;
  _shards: ShardInfo;
  hits: {
    total: {
      value: number;
      relation: string;
    };
    max_score: null;
    hits: Hit<TDocument>[];
  };
}

export interface UpdateResponse {
  _index: string;
  _id: string;
  _version: number;
  result: string;
  _shards: ShardInfo;
  _seq_no: number;
  _primary_term: number;
}
export type CreateResponse = UpdateResponse;

export interface IndexResponse {
  [indexName: string]: {
    mappings?: {
      properties: {
        [property: string]: IndexProperty;
      };
    };
  };
}

export interface IndexProperty {
  type?: string;
  fields?: {
    [property: string]: IndexProperty; // TODO: This is not quite right
  };
  properties?: {
    [property: string]: IndexProperty;
  };
}

export interface SortTerm {
  [x: string]: {
    order: 'asc' | 'desc';
  };
}

export interface Bucket {
  doc_count: number;
  key: string;
}

export const EMPTY_HIT = {
  _index: '',
  _id: '',
  _score: '',
  _source: {},
};
