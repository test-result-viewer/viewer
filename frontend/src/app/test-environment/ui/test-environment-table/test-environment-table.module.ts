import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { TableVirtualScrollModule } from 'ng-table-virtual-scroll';
import { TestEnvironmentTableComponent } from './test-environment-table.component';

@NgModule({
  declarations: [TestEnvironmentTableComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatTooltipModule,
    ScrollingModule,
    TableVirtualScrollModule,
  ],
  exports: [TestEnvironmentTableComponent],
})
export class TestEnvironmentTableModule {}
