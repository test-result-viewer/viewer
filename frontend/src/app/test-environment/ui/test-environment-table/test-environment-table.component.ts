import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TableConfiguration } from '../../data-access/test-environment.service';
import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';
import * as Utilities from 'src/app/shared/utilities/utilities';

@Component({
  selector: 'app-test-environment-table',
  templateUrl: './test-environment-table.component.html',
  styleUrls: ['./test-environment-table.component.scss'],
})
export class TestEnvironmentTableComponent {
  @Input() tableConfiguration?: TableConfiguration;
  @Output() resultClickEvent = new EventEmitter<Hit<Testresult>[]>();

  buildTooltipText(hits?: Hit<Testresult>[]): string {
    if (!hits || hits.length === 0) {
      return '';
    }

    const labels = hits
      .filter(
        h => h._source.labels !== undefined && h._source.labels.length > 0
      )
      .map(h => {
        return (
          'Labels:\n' +
            h._source.labels?.map(l => `${l.key}: ${l.value}`).join('\n') || ''
        );
      })
      .join('\n');

    const errors = hits
      .filter(h => h._source.result !== 'success')
      .map(
        h => `${h._source.failure?.message}\n${h._source.failure?.stacktrace}`
      )
      .join('\n\n');

    return `# of Jobs: ${hits.length}\n\n`.concat(labels, errors);
  }

  getOverallResult(data: Hit<Testresult>[]): string {
    if (!data) return '';
    const x = Utilities.isAllError(data);

    if (x === 'error') return x;

    const y = data.find(x => x._source.result === 'error');
    return y === undefined ? x : 'investigate';
  }

  getSymbol(result: string): string {
    return Utilities.getSymbol(result);
  }

  onResultClick(hits: Hit<Testresult>[]): void {
    this.resultClickEvent.emit(hits);
  }
}
