import { ComponentFixture, TestBed } from '@angular/core/testing';
import * as fc from 'fast-check';
import * as Utilities from 'src/app/shared/utilities/utilities';

import { TestEnvironmentTableComponent } from './test-environment-table.component';

describe('TestEnvironmentTableComponent', () => {
  let component: TestEnvironmentTableComponent;
  let fixture: ComponentFixture<TestEnvironmentTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestEnvironmentTableComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestEnvironmentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getSymbol', () => {
    it('should behave as in Utilities', () => {
      fc.assert(
        fc.property(
          fc.stringOf(
            fc.constantFrom(
              'success',
              'error',
              'failed',
              'ignored',
              'inconclusive'
            ),
            { minLength: 1, maxLength: 1 }
          ),
          text => component.getSymbol(text) === Utilities.getSymbol(text)
        )
      );
      fc.assert(
        fc.property(
          fc.string(),
          text => component.getSymbol(text) === Utilities.getSymbol(text)
        )
      );
    });
  });

  describe('getOverallResult', () => {
    it('getOverallResult should return success when all is success otherwise investigate', () => {
      const hitArbitrary = fc.array(
        fc.record({
          _index: fc.string(),
          _id: fc.string(),
          _score: fc.string(),
          _source: fc.record({
            result: fc.stringOf(
              fc.constantFrom(
                // 'success',
                'error'
                // 'failed',
                // 'ignored',
                // 'inconclusive'
              ),
              { minLength: 1, maxLength: 1 }
            ),
          }),
        })
      );

      const hitArbitraryRandomResult = fc.array(
        fc.record({
          _index: fc.string(),
          _id: fc.string(),
          _score: fc.string(),
          _source: fc.record({
            result: fc.string(),
          }),
        })
      );

      fc.assert(
        fc.property(hitArbitrary, hits => {
          component.getOverallResult(hits) === 'error';
        })
      );
      fc.assert(
        fc.property(
          hitArbitraryRandomResult,
          hits =>
            component.getOverallResult(hits) === Utilities.isAllError(hits)
        )
      );
    });
  });

  describe('buildTooltipText', () => {
    it("should return '' when array is undefined or empty", () => {
      expect(component.buildTooltipText([])).toBe('');
      expect(component.buildTooltipText(undefined)).toBe('');
    });

    it('should return text with single item in array', () => {
      const data = [
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'success',
            build: {
              version: 0,
              identifier: 'xxx',
              brand: '',
              number: 0,
              project: '',
              trigger: 'commit',
              url: '',
            },
          },
        },
      ];
      expect(component.buildTooltipText(data)).toEqual('# of Jobs: 1\n\n');
    });

    it('should return text with multiple items in array', () => {
      const data = [
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'success',
            build: {
              version: 0,
              identifier: 'xxx',
              brand: '',
              number: 0,
              project: '',
              trigger: 'commit',
              url: '',
            },
          },
        },
        {
          _index: '',
          _id: '',
          _score: '',
          _source: {
            result: 'error',
            build: {
              version: 1,
              identifier: 'yyy',
              brand: '',
              number: 0,
              project: '',
              trigger: 'commit',
              url: '',
            },
            failure: {
              message: 'message',
              stacktrace: 'stack trace',
            },
          },
        },
      ];
      expect(component.buildTooltipText(data)).toEqual(
        '# of Jobs: 2\n\nmessage\nstack trace'
      );
    });
  });
});
