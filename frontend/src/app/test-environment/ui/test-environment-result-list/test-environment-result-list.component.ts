import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';

@Component({
  selector: 'app-test-environment-result-list',
  templateUrl: './test-environment-result-list.component.html',
  styleUrls: ['./test-environment-result-list.component.scss'],
})
export class TestEnvironmentResultListComponent {
  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<TestEnvironmentResultListComponent>,
    @Inject(MAT_DIALOG_DATA) public resultHits: Hit<Testresult>[]
  ) {}
  navigateTo(id: string) {
    this.dialogRef.close();
    this.router.navigate(['/testresult-detail'], { queryParams: { id } });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
