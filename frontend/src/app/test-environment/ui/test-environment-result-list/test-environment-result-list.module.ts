import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { TestEnvironmentResultListComponent } from './test-environment-result-list.component';
import {
  MAT_DIALOG_DATA,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogModule,
} from '@angular/material/dialog';

@NgModule({
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
      },
    },
  ],
  declarations: [TestEnvironmentResultListComponent],
  imports: [CommonModule, MatButtonModule, MatDialogModule],
  exports: [TestEnvironmentResultListComponent],
})
export class TestEnvironmentResultListModule {}
