import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';

import { TestEnvironmentResultListComponent } from './test-environment-result-list.component';
import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';

describe('TestEnvironmentResultListComponent', () => {
  let component: TestEnvironmentResultListComponent;
  let fixture: ComponentFixture<TestEnvironmentResultListComponent>;

  let model: Hit<Testresult>[] = [
    {
      _id: '1',
      _index: 'testresult',
      _score: 1,
      _source: {} as Testresult,
    },
  ];


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [TestEnvironmentResultListComponent],
      providers: [
        { provide: MatDialog, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: model },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TestEnvironmentResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
