import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestEnvironmentFilterComponent } from './test-environment-filter.component';
import { fromEvent } from 'rxjs';

describe('TestEnvironmentFilterComponent', () => {
  let component: TestEnvironmentFilterComponent;
  let fixture: ComponentFixture<TestEnvironmentFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestEnvironmentFilterComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestEnvironmentFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onFilter should create new filter event', (done: DoneFn) => {
    component.queryFilter = 'result:success';
    component.filterEvent.subscribe(filter => {
      expect(filter.filter).toBe('result:success');
      done();
    })
    component.onFilter();
  });

  it('lastTwentyFourHours should create filter for it', (done: DoneFn) => {
    component.filterEvent.subscribe(filter => {
      expect(filter.filter).toBe('start-time:[now-1d/d TO *]');
      done();
    })
    component.lastTwentyFourHours();
  });

  it('lastSevenDays should create filter for it', (done: DoneFn) => {
    component.filterEvent.subscribe(filter => {
      expect(filter.filter).toBe('start-time:[now-7d/d TO *]');
      done();
    })
    component.lastSevenDays();
  });

  it('clearQuery should remove filter', (done: DoneFn) => {
    component.filterEvent.subscribe(filter => {
      expect(filter.filter).toBe('');
      done();
    })
    component.clearQuery();
  });
});
