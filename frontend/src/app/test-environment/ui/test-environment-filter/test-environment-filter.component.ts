import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { faShareFromSquare } from '@fortawesome/free-regular-svg-icons';

import { Filter } from 'src/app/types/testresults.types';

const QUERY_FILTER_FIELD = 'queryFilter';

@Component({
  selector: 'app-test-environment-filter',
  templateUrl: './test-environment-filter.component.html',
  styleUrls: ['./test-environment-filter.component.scss'],
})
export class TestEnvironmentFilterComponent {
  @Input()
  set queryFilter(filter: string) {
    this.filterForm.get(QUERY_FILTER_FIELD)?.setValue(filter);
  }
  get queryFilter() {
    return this.filterForm.get(QUERY_FILTER_FIELD)?.value;
  }

  @Output() filterEvent = new EventEmitter<Filter>();

  faShareFromSquare = faShareFromSquare;
  filterForm: FormGroup = new FormGroup([]);

  constructor() {
    this.filterForm = new FormGroup({
      [QUERY_FILTER_FIELD]: new FormControl(this.queryFilter),
    });
  }

  onFilter() {
    this.filterEvent.emit(this.createFilter());
  }

  lastTwentyFourHours() {
    const query = 'start-time:[now-1d/d TO *]';
    this.queryFilter = query;
    this.filterEvent.emit(this.createFilter());
  }

  lastSevenDays() {
    const query = 'start-time:[now-7d/d TO *]';
    this.queryFilter = query;
    this.filterEvent.emit(this.createFilter());
  }

  clearQuery() {
    const query = ``;
    this.queryFilter = query;
    this.filterEvent.emit(this.createFilter());
  }

  private createFilter(): Filter {
    return {
      filter: this.queryFilter,
      commentFilter: '',
      branch: '',
      numberOfCommits: 0,
      assignment: '',
    };
  }
}
