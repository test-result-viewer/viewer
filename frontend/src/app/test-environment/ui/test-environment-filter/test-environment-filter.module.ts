import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';

import { TestEnvironmentFilterComponent } from './test-environment-filter.component';

@NgModule({
  declarations: [TestEnvironmentFilterComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    FontAwesomeModule,
    MatFormFieldModule,
    ReactiveFormsModule,
  ],
  exports: [TestEnvironmentFilterComponent],
})
export class TestEnvironmentFilterModule {}
