import { Injectable } from '@angular/core';
import {
  Observable,
  combineLatest,
  map,
  of,
  switchMap,
} from 'rxjs';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { Testresult, Filter } from 'src/app/types/testresults.types';
import { Hit, SearchResponse } from 'src/app/types/elasticsearch.types';

export interface TableRowData {
  testsuite: string | undefined;
  testcase: string;
  results: { [x: string]: Hit<Testresult>[] };
}

export interface TableData {
  devicesByDate: Map<string, Set<string>>;
  dataSource: TableRowData[];
  dates: string[];
  cols: Map<string, Column>;
}

export interface TableConfiguration {
  data: TableData;
  columns: Column[];
  displayedColumns: string[];
  headerGroups: string[];
}

export interface Column {
  columnDef: string;
  header: string;
}

interface MinimalDateResponse extends SearchResponse {
  aggregations: {
    byStartTime: {
      buckets: {
        key: number;
        key_as_string: string;
        doc_count: number;
      }[];
    };
  };
}

interface EnvironmentHistoryResponse extends SearchResponse {
  aggregations: {
    byTestsuite: {
      buckets: {
        key: string;
        doc_count: number;
        byTestcase: {
          buckets: {
            key: string;
            doc_count: number;
            byStartTime: {
              buckets: {
                key: number;
                key_as_string: string;
                doc_count: number;
                byDevices: {
                  buckets: {
                    key: string;
                    doc_count: number;
                    doc: {
                      hits: {
                        total: { value: number; relation: string };
                        max_score: number;
                        hits: Hit<Testresult>[];
                      };
                    };
                  }[];
                };
              }[];
            };
          }[];
        };
      }[];
    };
  };
}

@Injectable({
  providedIn: 'root',
})
export class TestEnvironmentService {
  constructor(private elasticsearch: ElasticsearchService) {}

  getTableData(filter$: Observable<Filter>): Observable<TableData> {
    return filter$.pipe(
      switchMap(filter =>
        // TODO: concat somehow
        combineLatest([of(filter), this.getOldestDate(filter)])
      ),
      switchMap(([filter, startTime]) =>
        this.getTestEnvironmentHistory(startTime, filter)
      ),
      map(response => {
        const [dataSource, cols, devicesByDate] = this.getDataSource(response);
        const dates = Array.from(devicesByDate.keys()).sort((a, b) =>
          b.localeCompare(a)
        );

        return {
          devicesByDate,
          dataSource: dataSource,
          cols,
          dates,
        };
      }),
    );
  }

  private getOldestDate(filter: Filter, size?: number): Observable<Date> {
    const params: { size?: number; must: object } = {
      size,
      must: [],
    };

    if (filter.filter) {
      params['must'] = [
        {
          query_string: {
            query: filter.filter,
          },
        },
      ];
    }

    return this.elasticsearch
      .templateSearch<MinimalDateResponse>(
        'testresult',
        'get-oldest-date',
        params
      )
      .pipe(
        map(response => {
          const startTimeBuckets = response.aggregations.byStartTime.buckets;
          return startTimeBuckets.length == 0
            ? new Date()
            : new Date(
                startTimeBuckets[startTimeBuckets.length - 1].key_as_string
              );
        })
      );
  }

  private getTestEnvironmentHistory(
    startTime: Date,
    filter: Filter
  ): Observable<EnvironmentHistoryResponse> {
    const params: { startTime: string; must: object } = {
      startTime: startTime.toISOString(),
      must: [],
    };
    if (filter.filter) {
      params['must'] = [
        {
          query_string: {
            query: filter.filter,
          },
        },
      ];
    }

    return this.elasticsearch
      .templateSearch<EnvironmentHistoryResponse>(
        'testresult',
        'get-test-environment-history',
        params
      )
  }

  private getDataSource(
    response: EnvironmentHistoryResponse
  ): [TableRowData[], Map<string, Column>, Map<string, Set<string>>] {
    const cols = new Map<string, Column>();
    const devicesByDate = new Map<string, Set<string>>();
    let data: TableRowData[] = [];
    response.aggregations.byTestsuite.buckets.forEach(testsuiteBucket => {
      data = data.concat(
        testsuiteBucket.byTestcase.buckets.map(testCaseBucket => {
          const results = new Map<string, Hit<Testresult>[]>();

          testCaseBucket.byStartTime.buckets.forEach(startTimeBucket => {
            startTimeBucket.byDevices.buckets.forEach(deviceBucket => {
              const key = `${startTimeBucket.key_as_string}-${deviceBucket.key}`;
              if (!cols.has(key)) {
                cols.set(key, { columnDef: key, header: deviceBucket.key });
              }
              if (!devicesByDate.has(startTimeBucket.key_as_string)) {
                devicesByDate.set(startTimeBucket.key_as_string, new Set());
              }

              startTimeBucket.byDevices.buckets.forEach(b =>
                devicesByDate.get(startTimeBucket.key_as_string)?.add(b.key)
              );
              results.set(
                key,
                deviceBucket.doc.hits.hits.map(hit => {
                  hit._source.result = hit._source.result?.toLowerCase();
                  return hit;
                })
              );
            });
          });

          return {
            testsuite: testsuiteBucket.key,
            testcase: testCaseBucket.key,
            results: {
              ...Object.fromEntries(results.entries()),
            },
          };
        })
      );
    });

    return [data, cols, devicesByDate];
  }
}
