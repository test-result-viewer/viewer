import { TestBed } from '@angular/core/testing';

import { TestEnvironmentService } from './test-environment.service';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Filter } from 'src/app/types/testresults.types';

describe('TestEnvironmentService', () => {
  let service: TestEnvironmentService;
  let elasticsearchService: ElasticsearchService;
  let templateSearchSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: {} }],
    });

    function fakeCall<T>(index: string, template: string): Observable<T> {
      if (template === 'get-oldest-date') {
        return of({
          took: 2,
          timed_out: false,
          _shards: {
            total: 1,
            successful: 1,
            skipped: 0,
            failed: 0,
          },
          hits: {
            total: {
              value: 1377,
              relation: 'eq',
            },
            max_score: null,
            hits: [],
          },
          aggregations: {
            byStartTime: {
              doc_count_error_upper_bound: 0,
              sum_other_doc_count: 1235,
              buckets: [
                {
                  key: 1677615152000,
                  key_as_string: '2023-02-28T20:12:32.000Z',
                  doc_count: 50,
                },
                {
                  key: 1677528752000,
                  key_as_string: '2023-02-27T20:12:32.000Z',
                  doc_count: 46,
                },
                {
                  key: 1677442352000,
                  key_as_string: '2023-02-26T20:12:32.000Z',
                  doc_count: 46,
                },
              ],
            },
          },
        } as T);
      }

      if (template === 'get-test-environment-history') {
        return of({
          took: 8,
          timed_out: false,
          _shards: {
            total: 1,
            successful: 1,
            skipped: 0,
            failed: 0,
          },
          hits: {
            total: {
              value: 142,
              relation: 'eq',
            },
            max_score: null,
            hits: [],
          },
          aggregations: {
            byTestsuite: {
              buckets: [
                {
                  key: 'suite3',
                  doc_count: 26,
                  byTestcase: {
                    buckets: [
                      {
                        key: 'test case 8',
                        doc_count: 4,
                        byStartTime: {
                          buckets: [
                            {
                              key: 1677615152000,
                              key_as_string: '2023-02-28T20:12:32.000Z',
                              doc_count: 2,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-A',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: '85mXs4cB4ApC2rW2pmOB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 8',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-28T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 107,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'somerepo',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '1d5b251bb80f0406cee5f26e285733276b60a757',
                                                  'commit-time':
                                                    '2022-12-01T03:05:16.141323Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'someotherrepo',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '4dd276c500d4460d809bf9fce12e1c939b445424',
                                                  'commit-time':
                                                    '2022-11-03T22:55:23.334145Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-A',
                                                capabilities: ['hardware #1'],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\path\\to\\src',
                                                user: 'Darek',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.30319.4200',
                                                'os-version': 'SomeOSVersion',
                                                'machine-name': 'Server123',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 5.914,
                                              asserts: 0,
                                              categories: [
                                                'StageSmoke',
                                                'StageSmoke',
                                              ],
                                              properties: [
                                                {
                                                  name: 'property2',
                                                  value: '10',
                                                },
                                                {
                                                  name: 'property3',
                                                  value: '10',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                  {
                                    key: 'host-C',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'l5mXs4cB4ApC2rW2pmKA',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 8',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-28T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 61,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'test',
                                                  repo: 'reponame',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '37145b2ebb9134925629c327be3fd28fd55602c2',
                                                  'commit-time':
                                                    '2022-08-05T18:26:06.362882Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-C',
                                                capabilities: [
                                                  'hardware #1',
                                                  'label #2',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Thomas',
                                                'framework-version':
                                                  '2.3.4.5.6',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version': 'Win7',
                                                'machine-name': 'this-PC',
                                                'user-domain': 'COMPANY1',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'de-DE',
                                              },
                                              executed: true,
                                              result: 'ignored',
                                              time: 9.056,
                                              asserts: 0,
                                              categories: [
                                                'SpecialBuilds',
                                                'StageSmoke',
                                              ],
                                              properties: [
                                                {
                                                  name: 'propery1',
                                                  value: '10',
                                                },
                                              ],
                                              labels: [],
                                              reason: 'some explanation',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                            {
                              key: 1677528752000,
                              key_as_string: '2023-02-27T20:12:32.000Z',
                              doc_count: 2,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-B',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: '_JmXs4cB4ApC2rW2pmSX',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 8',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-27T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 232,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'repo4',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '69429d13171acf02356268de33206f4292db3b56',
                                                  'commit-time':
                                                    '2023-03-13T04:28:32.455964Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'reponame',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '3b1ef75b3d56d36195751effd953ed487e91cd33',
                                                  'commit-time':
                                                    '2022-05-06T20:00:11.678854Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-B',
                                                capabilities: ['hardware #3'],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\path\\to\\src',
                                                user: 'Andreas',
                                                'framework-version':
                                                  '1.2.3.4.5',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version':
                                                  'Microsoft Windows NT 6.2.9200.0',
                                                'machine-name': 'that-PC',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-CH',
                                                'current-uiculture': 'de-DE',
                                              },
                                              executed: true,
                                              result: 'inconclusive',
                                              time: 2.725,
                                              asserts: 0,
                                              categories: ['StageSmoke'],
                                              properties: [
                                                {
                                                  name: 'propery1',
                                                  value: 'someothervalue',
                                                },
                                                {
                                                  name: 'property3',
                                                  value: 'someothervalue',
                                                },
                                              ],
                                              labels: [],
                                              reason: 'reason for failure',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                  {
                                    key: 'host-C',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'XpmXs4cB4ApC2rW2pmSB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 8',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-27T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 106,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'test',
                                                  repo: 'reponame',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '96d005dd459b02b6482073067991cfa399b189d0',
                                                  'commit-time':
                                                    '2022-10-26T01:10:02.000035Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'someotherrepo',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '7db3eb45c0125ae77809748371ea877bcde99db5',
                                                  'commit-time':
                                                    '2022-08-02T19:46:08.353301Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-C',
                                                capabilities: [
                                                  'label #2',
                                                  'label #1',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\path\\to\\src',
                                                user: 'Darek',
                                                'framework-version':
                                                  '1.2.3.4.5',
                                                'clr-version': '3.0.1234.5678',
                                                'os-version': 'SomeOSVersion',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'de-DE',
                                              },
                                              executed: true,
                                              result: 'failed',
                                              time: 2.388,
                                              asserts: 0,
                                              categories: [
                                                'SpecialBuilds',
                                                'HealthChecks',
                                              ],
                                              properties: [
                                                {
                                                  name: 'property2',
                                                  value: '10',
                                                },
                                                {
                                                  name: 'propery1',
                                                  value: '123456',
                                                },
                                              ],
                                              labels: [],
                                              reason: 'reason for failure',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                      {
                        key: 'test case 4',
                        doc_count: 3,
                        byStartTime: {
                          buckets: [
                            {
                              key: 1677615152000,
                              key_as_string: '2023-02-28T20:12:32.000Z',
                              doc_count: 1,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-C',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'UJmXs4cB4ApC2rW2pmWY',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 4',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-28T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 113,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'repo4',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'f173290151824aa546189a1432d7a13cab0647ab',
                                                  'commit-time':
                                                    '2022-07-23T08:21:42.942010Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-C',
                                                capabilities: [
                                                  'hardware #2',
                                                  'label #3',
                                                  'hardware #1',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\path\\to\\src',
                                                user: 'Darek',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version': 'Win7',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'domain3',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-DE',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 5.521,
                                              asserts: 0,
                                              categories: ['SpecialBuilds'],
                                              properties: [
                                                {
                                                  name: 'property3',
                                                  value: 'false',
                                                },
                                                {
                                                  name: 'property2',
                                                  value: 'true',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                            {
                              key: 1677528752000,
                              key_as_string: '2023-02-27T20:12:32.000Z',
                              doc_count: 1,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-A',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'UZmXs4cB4ApC2rW2pmWY',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 4',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-27T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 205,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'repo4',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'f173290151824aa546189a1432d7a13cab0647ab',
                                                  'commit-time':
                                                    '2022-07-23T08:21:42.942010Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-A',
                                                capabilities: ['label #2'],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Andreas',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '3.0.1234.5678',
                                                'os-version': 'Win7',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'domain3',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-CH',
                                                'current-uiculture': 'en-US',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 7.806,
                                              asserts: 0,
                                              categories: ['SpecialBuilds'],
                                              properties: [
                                                {
                                                  name: 'property2',
                                                  value: '123456',
                                                },
                                                {
                                                  name: 'property2',
                                                  value: '123456',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                            {
                              key: 1677442352000,
                              key_as_string: '2023-02-26T20:12:32.000Z',
                              doc_count: 1,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-C',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'HJmXs4cB4ApC2rW2pmKA',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 4',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 43,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'dev',
                                                  repo: 'reponame',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '926417633348f96ef13f1dfd41c7097ae3959d89',
                                                  'commit-time':
                                                    '2023-02-08T14:49:46.248656Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-C',
                                                capabilities: [
                                                  'label #1',
                                                  'label #3',
                                                  'hardware #1',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Darek',
                                                'framework-version':
                                                  '2.3.4.5.6',
                                                'clr-version': '3.0.1234.5678',
                                                'os-version': 'SomeOSVersion',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'domain3',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 11.468,
                                              asserts: 0,
                                              categories: ['SpecialBuilds'],
                                              properties: [
                                                {
                                                  name: 'propery1',
                                                  value: 'false',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                      {
                        key: 'test case 10',
                        doc_count: 2,
                        byStartTime: {
                          buckets: [
                            {
                              key: 1677615152000,
                              key_as_string: '2023-02-28T20:12:32.000Z',
                              doc_count: 1,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-C',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: '_5mXs4cB4ApC2rW2pmKB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 10',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-28T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 192,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'someotherrepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '6bf5c871dad7351ad19a1df5c97f68eae8f75232',
                                                  'commit-time':
                                                    '2022-10-10T03:51:48.974752Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-C',
                                                capabilities: [
                                                  'label #2',
                                                  'label #3',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Thomas',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.78962.3698',
                                                'os-version': 'Win7',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'COMPANY1',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-DE',
                                                'current-uiculture': 'en-US',
                                              },
                                              executed: false,
                                              result: 'ignored',
                                              time: 10.806,
                                              asserts: 0,
                                              categories: [
                                                'HealthChecks',
                                                'SpecialBuilds',
                                              ],
                                              properties: [
                                                {
                                                  name: 'propery1',
                                                  value: '10',
                                                },
                                                {
                                                  name: 'property2',
                                                  value: '10',
                                                },
                                              ],
                                              labels: [],
                                              reason:
                                                'reason for there being spiders',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                            {
                              key: 1677442352000,
                              key_as_string: '2023-02-26T20:12:32.000Z',
                              doc_count: 1,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-B',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'cZmXs4cB4ApC2rW2pmSB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 10',
                                              testsuite: 'suite3',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 198,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'branch2',
                                                  repo: 'someotherrepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'd034141139a39ba42bf5cdc3eae4d412d1188aac',
                                                  'commit-time':
                                                    '2023-03-04T05:21:21.578466Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-B',
                                                capabilities: [
                                                  'hardware #2',
                                                  'label #1',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Thomas',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version': 'Win7',
                                                'machine-name': 'this-PC',
                                                'user-domain': 'domain3',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-DE',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 3.424,
                                              asserts: 0,
                                              categories: [
                                                'StageSmoke',
                                                'SpecialBuilds',
                                              ],
                                              properties: [
                                                {
                                                  name: 'propery1',
                                                  value: 'false',
                                                },
                                                {
                                                  name: 'propery1',
                                                  value: 'someothervalue',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
                {
                  key: 'suite6',
                  doc_count: 25,
                  byTestcase: {
                    buckets: [
                      {
                        key: 'test case 22',
                        doc_count: 3,
                        byStartTime: {
                          buckets: [
                            {
                              key: 1677615152000,
                              key_as_string: '2023-02-28T20:12:32.000Z',
                              doc_count: 2,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-A',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'aJmXs4cB4ApC2rW2pmOB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 22',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-28T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 155,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'repo4',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '5f444e8a37c4f826dff4d2cdbf217afee01756f1',
                                                  'commit-time':
                                                    '2023-02-23T18:04:44.525681Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'somerepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'c007ac6b9a2e7c51192cd106e03136ff9b3c37d5',
                                                  'commit-time':
                                                    '2022-05-12T22:27:22.325027Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-A',
                                                capabilities: [
                                                  'label #2',
                                                  'hardware #1',
                                                  'hardware #2',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\path\\to\\src',
                                                user: 'Darek',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.78962.3698',
                                                'os-version':
                                                  'Microsoft Windows NT 6.2.9200.0',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'en-US',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 3.217,
                                              asserts: 0,
                                              categories: ['HealthChecks'],
                                              properties: [
                                                {
                                                  name: 'property3',
                                                  value: 'true',
                                                },
                                                {
                                                  name: 'property3',
                                                  value: '123456',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                  {
                                    key: 'host-B',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'SJmXs4cB4ApC2rW2pmSB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 22',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-28T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 202,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'dev',
                                                  repo: 'someotherrepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'c936a6e12468abed4ad523724b72d4edf7485304',
                                                  'commit-time':
                                                    '2022-06-12T23:50:09.677262Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'repo4',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '30da2305f6777a7fa0d6d73ecce9ce2c5eeb49c5',
                                                  'commit-time':
                                                    '2023-04-11T04:29:43.443649Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-B',
                                                capabilities: [
                                                  'hardware #3',
                                                  'label #1',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Darek',
                                                'framework-version':
                                                  '1.2.3.4.5',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version':
                                                  'Microsoft Windows NT 6.2.9200.0',
                                                'machine-name': 'this-PC',
                                                'user-domain': 'domain3',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'failed',
                                              time: 8.022,
                                              asserts: 0,
                                              categories: [
                                                'HealthChecks',
                                                'SpecialBuilds',
                                              ],
                                              properties: [
                                                {
                                                  name: 'property3',
                                                  value: '123456',
                                                },
                                                {
                                                  name: 'propery1',
                                                  value: '10',
                                                },
                                              ],
                                              labels: [],
                                              reason: 'some explanation',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                            {
                              key: 1677442352000,
                              key_as_string: '2023-02-26T20:12:32.000Z',
                              doc_count: 1,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-A',
                                    doc_count: 1,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 1,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: '8JmXs4cB4ApC2rW2pmKB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 22',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 180,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'somerepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '2ddb9bab4867509b19b9cf9837d42af749cc7f5a',
                                                  'commit-time':
                                                    '2022-08-18T11:12:17.978868Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'reponame',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'b691bbed928492edbe39dbed5a1499d67cb6e165',
                                                  'commit-time':
                                                    '2022-09-30T13:31:46.318017Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-A',
                                                capabilities: [
                                                  'label #2',
                                                  'label #3',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Thomas',
                                                'framework-version':
                                                  '2.3.4.5.6',
                                                'clr-version': '4.0.30319.4200',
                                                'os-version': 'SomeOSVersion',
                                                'machine-name': 'that-PC',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-CH',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 11.615,
                                              asserts: 0,
                                              categories: [
                                                'HealthChecks',
                                                'HealthChecks',
                                              ],
                                              properties: [
                                                {
                                                  name: 'property2',
                                                  value: 'false',
                                                },
                                                {
                                                  name: 'propery1',
                                                  value: 'true',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                      {
                        key: 'test case 18',
                        doc_count: 2,
                        byStartTime: {
                          buckets: [
                            {
                              key: 1677442352000,
                              key_as_string: '2023-02-26T20:12:32.000Z',
                              doc_count: 2,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-A',
                                    doc_count: 2,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 2,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: 'rpmXs4cB4ApC2rW2pmOB',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 18',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 106,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'dev',
                                                  repo: 'somerepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    'cc06f34d7bac4041291c7a7f0abb153d712c3c5c',
                                                  'commit-time':
                                                    '2023-02-28T07:10:52.145387Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'test',
                                                  repo: 'repo4',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '6d646ba00016b0ab71e34933c6309b882f9ecfbb',
                                                  'commit-time':
                                                    '2022-11-14T02:14:09.395910Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-A',
                                                capabilities: [
                                                  'hardware #1',
                                                  'label #2',
                                                  'hardware #2',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Darek',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.78962.3698',
                                                'os-version': 'Win7',
                                                'machine-name': 'that-PC',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'de-CH',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 9.891,
                                              asserts: 0,
                                              categories: ['HealthChecks'],
                                              properties: [
                                                {
                                                  name: 'property2',
                                                  value: 'true',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                          {
                                            _index: 'testresult',
                                            _id: '1ZmXs4cB4ApC2rW2pmWY',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 18',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 133,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'someotherrepo',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '322e974df0da51b1b9e5116a93ce1d3c78830f02',
                                                  'commit-time':
                                                    '2022-10-25T07:12:45.507301Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'dev',
                                                  repo: 'reponame',
                                                  type: 'svn',
                                                  'commit-hash':
                                                    '25496725cf1ddba96e7c1a2a6dba4b9563c8e10e',
                                                  'commit-time':
                                                    '2023-02-16T02:34:33.292688Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-A',
                                                capabilities: [
                                                  'hardware #1',
                                                  'label #2',
                                                  'hardware #3',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'UNIX',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Darek',
                                                'framework-version':
                                                  '1.2.3.4.5',
                                                'clr-version': '3.0.1234.5678',
                                                'os-version': 'Win7',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'domain2',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-DE',
                                                'current-uiculture': 'en-US',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 6.831,
                                              asserts: 0,
                                              categories: [
                                                'SpecialBuilds',
                                                'StageSmoke',
                                              ],
                                              properties: [
                                                {
                                                  name: 'property2',
                                                  value: 'false',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                      {
                        key: 'test case 29',
                        doc_count: 2,
                        byStartTime: {
                          buckets: [
                            {
                              key: 1677442352000,
                              key_as_string: '2023-02-26T20:12:32.000Z',
                              doc_count: 2,
                              byDevices: {
                                buckets: [
                                  {
                                    key: 'host-B',
                                    doc_count: 2,
                                    doc: {
                                      hits: {
                                        total: {
                                          value: 2,
                                          relation: 'eq',
                                        },
                                        max_score: 1,
                                        hits: [
                                          {
                                            _index: 'testresult',
                                            _id: '9pmXs4cB4ApC2rW2pmSX',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 29',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 39,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'prod',
                                                  repo: 'repo4',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '69429d13171acf02356268de33206f4292db3b56',
                                                  'commit-time':
                                                    '2023-03-13T04:28:32.455964Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'reponame',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '3b1ef75b3d56d36195751effd953ed487e91cd33',
                                                  'commit-time':
                                                    '2022-05-06T20:00:11.678854Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-B',
                                                capabilities: [
                                                  'label #2',
                                                  'label #3',
                                                  'label #1',
                                                ],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\test\\directory',
                                                user: 'Thomas',
                                                'framework-version':
                                                  '1.2.3.4.5',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version': 'SomeOSVersion',
                                                'machine-name': 'someOtherPC',
                                                'user-domain': 'COMPANY1',
                                              },
                                              'culture-info': {
                                                'current-culture': 'en-US',
                                                'current-uiculture': 'en-US',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 3.015,
                                              asserts: 0,
                                              categories: ['StageSmoke'],
                                              properties: [
                                                {
                                                  name: 'property3',
                                                  value: 'true',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                          {
                                            _index: 'testresult',
                                            _id: 'M5mXs4cB4ApC2rW2pmKA',
                                            _score: 1,
                                            _source: {
                                              testcase: 'test case 29',
                                              testsuite: 'suite6',
                                              description:
                                                'some description to be randomized',
                                              'start-time':
                                                '2023-02-26T20:12:32.000000Z',
                                              build: {
                                                identifier: 'build id',
                                                number: 102,
                                                project: 'project',
                                                trigger: 'trigger',
                                                url: 'url',
                                              },
                                              'version-control-system-info': [
                                                {
                                                  branch: 'dev',
                                                  repo: 'somerepo',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '086fc59043368700d56c880143f6e48cf1cfafb7',
                                                  'commit-time':
                                                    '2023-01-26T23:23:47.168791Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                                {
                                                  branch: 'prod',
                                                  repo: 'somerepo',
                                                  type: 'git',
                                                  'commit-hash':
                                                    '551d2e1a88a7950bd930435a0db7b44c3fde0ee8',
                                                  'commit-time':
                                                    '2022-05-24T17:11:21.179297Z',
                                                  'commit-author': 'Author',
                                                  'commit-message': 'message',
                                                },
                                              ],
                                              'test-environment': {
                                                name: 'host-B',
                                                capabilities: ['hardware #3'],
                                              },
                                              environment: {
                                                framework: 'nunit',
                                                platform: 'Win32NT',
                                                cwd: 'C:\\path\\to\\src',
                                                user: 'Darek',
                                                'framework-version':
                                                  '2.6.4.20092',
                                                'clr-version': '4.0.12345.2100',
                                                'os-version':
                                                  'Microsoft Windows NT 6.2.9200.0',
                                                'machine-name': 'that-PC',
                                                'user-domain': 'COMPANY1',
                                              },
                                              'culture-info': {
                                                'current-culture': 'de-CH',
                                                'current-uiculture': 'de-DE',
                                              },
                                              executed: true,
                                              result: 'success',
                                              time: 7.238,
                                              asserts: 0,
                                              categories: [
                                                'SpecialBuilds',
                                                'SpecialBuilds',
                                              ],
                                              properties: [
                                                {
                                                  name: 'propery1',
                                                  value: '123456',
                                                },
                                              ],
                                              labels: [],
                                              reason: '',
                                              failure: {
                                                message: '',
                                                stacktrace: '',
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        } as T);
      }

      return of({} as T);
    }

    elasticsearchService = TestBed.inject(ElasticsearchService);
    templateSearchSpy = spyOn(
      elasticsearchService,
      'templateSearch'
    ).and.callFake(fakeCall);

    service = TestBed.inject(TestEnvironmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return table data', (done: DoneFn) => {
    service.getTableData(of({ filter: '' } as Filter)).subscribe(r => {
      expect(r.dataSource[0].testsuite).toBe('suite3');
      expect(r.dataSource[0].testcase).toBe('test case 8');
      expect(Object.keys(r.dataSource[0].results).length).toBe(4);
      expect(
        r.dataSource[0].results['2023-02-28T20:12:32.000Z-host-A'].length
      ).toBe(1);
      expect(
        r.dataSource[0].results['2023-02-28T20:12:32.000Z-host-A'][0]._id
      ).toBe('85mXs4cB4ApC2rW2pmOB');
      expect(
        r.dataSource[0].results['2023-02-28T20:12:32.000Z-host-C'].length
      ).toBe(1);
      expect(
        r.dataSource[0].results['2023-02-28T20:12:32.000Z-host-C'][0]._id
      ).toBe('l5mXs4cB4ApC2rW2pmKA');
      expect(
        r.dataSource[0].results['2023-02-27T20:12:32.000Z-host-B'].length
      ).toBe(1);
      expect(
        r.dataSource[0].results['2023-02-27T20:12:32.000Z-host-B'][0]._id
      ).toBe('_JmXs4cB4ApC2rW2pmSX');
      expect(
        r.dataSource[0].results['2023-02-27T20:12:32.000Z-host-C'].length
      ).toBe(1);
      expect(
        r.dataSource[0].results['2023-02-27T20:12:32.000Z-host-C'][0]._id
      ).toBe('XpmXs4cB4ApC2rW2pmSB');

      expect(r.dataSource[1].testsuite).toBe('suite3');
      expect(r.dataSource[1].testcase).toBe('test case 4');
      expect(Object.keys(r.dataSource[1].results).length).toBe(3);
      expect(
        r.dataSource[1].results['2023-02-28T20:12:32.000Z-host-C'].length
      ).toBe(1);
      expect(
        r.dataSource[1].results['2023-02-28T20:12:32.000Z-host-C'][0]._id
      ).toBe('UJmXs4cB4ApC2rW2pmWY');
      expect(
        r.dataSource[1].results['2023-02-27T20:12:32.000Z-host-A'].length
      ).toBe(1);
      expect(
        r.dataSource[1].results['2023-02-27T20:12:32.000Z-host-A'][0]._id
      ).toBe('UZmXs4cB4ApC2rW2pmWY');
      expect(
        r.dataSource[1].results['2023-02-26T20:12:32.000Z-host-C'].length
      ).toBe(1);
      expect(
        r.dataSource[1].results['2023-02-26T20:12:32.000Z-host-C'][0]._id
      ).toBe('HJmXs4cB4ApC2rW2pmKA');

      expect(r.dataSource[2].testsuite).toBe('suite3');
      expect(r.dataSource[2].testcase).toBe('test case 10');
      expect(Object.keys(r.dataSource[2].results).length).toBe(2);
      expect(
        r.dataSource[2].results['2023-02-28T20:12:32.000Z-host-C'].length
      ).toBe(1);
      expect(
        r.dataSource[2].results['2023-02-28T20:12:32.000Z-host-C'][0]._id
      ).toBe('_5mXs4cB4ApC2rW2pmKB');
      expect(
        r.dataSource[2].results['2023-02-26T20:12:32.000Z-host-B'].length
      ).toBe(1);
      expect(
        r.dataSource[2].results['2023-02-26T20:12:32.000Z-host-B'][0]._id
      ).toBe('cZmXs4cB4ApC2rW2pmSB');

      expect(r.dataSource[3].testsuite).toBe('suite6');
      expect(r.dataSource[3].testcase).toBe('test case 22');
      expect(Object.keys(r.dataSource[3].results).length).toBe(3);
      expect(
        r.dataSource[3].results['2023-02-28T20:12:32.000Z-host-A'].length
      ).toBe(1);
      expect(
        r.dataSource[3].results['2023-02-28T20:12:32.000Z-host-A'][0]._id
      ).toBe('aJmXs4cB4ApC2rW2pmOB');
      expect(
        r.dataSource[3].results['2023-02-28T20:12:32.000Z-host-B'].length
      ).toBe(1);
      expect(
        r.dataSource[3].results['2023-02-28T20:12:32.000Z-host-B'][0]._id
      ).toBe('SJmXs4cB4ApC2rW2pmSB');
      expect(
        r.dataSource[3].results['2023-02-26T20:12:32.000Z-host-A'].length
      ).toBe(1);
      expect(
        r.dataSource[3].results['2023-02-26T20:12:32.000Z-host-A'][0]._id
      ).toBe('8JmXs4cB4ApC2rW2pmKB');

      expect(r.dataSource[4].testsuite).toBe('suite6');
      expect(r.dataSource[4].testcase).toBe('test case 18');
      expect(Object.keys(r.dataSource[4].results).length).toBe(1);
      expect(
        r.dataSource[4].results['2023-02-26T20:12:32.000Z-host-A'].length
      ).toBe(2);
      expect(
        r.dataSource[4].results['2023-02-26T20:12:32.000Z-host-A'][0]._id
      ).toBe('rpmXs4cB4ApC2rW2pmOB');
      expect(
        r.dataSource[4].results['2023-02-26T20:12:32.000Z-host-A'][1]._id
      ).toBe('1ZmXs4cB4ApC2rW2pmWY');

      expect(r.dataSource[5].testsuite).toBe('suite6');
      expect(r.dataSource[5].testcase).toBe('test case 29');
      expect(Object.keys(r.dataSource[5].results).length).toBe(1);
      expect(
        r.dataSource[5].results['2023-02-26T20:12:32.000Z-host-B'].length
      ).toBe(2);
      expect(
        r.dataSource[5].results['2023-02-26T20:12:32.000Z-host-B'][0]._id
      ).toBe('9pmXs4cB4ApC2rW2pmSX');
      expect(
        r.dataSource[5].results['2023-02-26T20:12:32.000Z-host-B'][1]._id
      ).toBe('M5mXs4cB4ApC2rW2pmKA');

      done();
    });
  });
});
