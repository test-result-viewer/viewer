import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { TestEnvironmentRoutingModule } from './test-environment-routing.module';
import { TestEnvironmentComponent } from './test-environment.component';
import { TestEnvironmentTableModule } from '../ui/test-environment-table/test-environment-table.module';
import { TestEnvironmentFilterModule } from '../ui/test-environment-filter/test-environment-filter.module';
import { TestEnvironmentResultListModule } from '../ui/test-environment-result-list/test-environment-result-list.module';


@NgModule({
  declarations: [TestEnvironmentComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    TestEnvironmentRoutingModule,
    TestEnvironmentTableModule,
    TestEnvironmentFilterModule,
    TestEnvironmentResultListModule,
  ],
})
export class TestEnvironmentModule {}
