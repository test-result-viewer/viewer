import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestEnvironmentComponent } from './test-environment.component';

const routes: Routes = [
  {
    path: '',
    component: TestEnvironmentComponent,
    title: 'Test Environment - Test Result Viewer',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestEnvironmentRoutingModule {}
