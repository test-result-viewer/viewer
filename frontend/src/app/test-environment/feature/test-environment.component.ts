import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, catchError, map, of } from 'rxjs';
import { Hit } from 'src/app/types/elasticsearch.types';
import { Testresult, Filter } from 'src/app/types/testresults.types';
import {
  TestEnvironmentService,
  TableData,
  TableConfiguration,
  Column,
} from '../data-access/test-environment.service';
import { TestEnvironmentResultListComponent } from '../ui/test-environment-result-list/test-environment-result-list.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { getHTTPErrorAsString, errorSnackBar } from 'src/app/shared/utilities/utilities';
import { LoadingService } from 'src/app/shared/data-access/loading/loading.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

const DEFAULT_COLUMNS = ['testsuite', 'testcase'];
const DEFAULT_FILTER = {
  filter: '',
  branch: '',
  assignment: '',
  commentFilter: '',
  numberOfCommits: 100,
};
const DEFAULT_CONFIG = {
  data: {} as TableData,
  columns: [] as Column[],
  displayedColumns: [] as string[],
  headerGroups: [] as string[],
};

@Component({
  selector: 'app-test-environment',
  templateUrl: './test-environment.component.html',
  styleUrls: ['./test-environment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestEnvironmentComponent implements OnInit {
  filter$: Observable<Filter> = of(DEFAULT_FILTER);
  tableData$?: Observable<TableData>;
  tableConfiguration$!: Observable<TableConfiguration>;
  dialogRef!: MatDialogRef<TestEnvironmentResultListComponent>;

  constructor(
    private service: TestEnvironmentService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    this.filter$ = this.getFilter().pipe(
      catchError((msg: string ) => {
        errorSnackBar(this.snackBar, msg);
        return of(DEFAULT_FILTER);
      })
    );
    this.tableData$ = this.service.getTableData(this.filter$);
    this.tableConfiguration$ = this.tableData$.pipe(
      map(tableData => {
        const headerGroups = ['header-row-first-group'].concat(
          tableData.dates.map(d => `header-row-${d}-group`)
        );

        const columns = Array.from(tableData.cols.values()).sort((a, b) => {
          return b.columnDef.localeCompare(a.columnDef);
        });

        const displayedColumns = DEFAULT_COLUMNS.concat(
          columns.map(d => d.columnDef)
        );

        return {
          data: tableData,
          columns,
          displayedColumns,
          headerGroups,
        };
      }),
      catchError(error => {
        errorSnackBar(this.snackBar, getHTTPErrorAsString(error));
        return of(DEFAULT_CONFIG);
      })
    );
  }

  getFilter(): Observable<Filter> {
    return this.activeRoute.queryParamMap.pipe(
      map(params => {
        const q = params.get('q');
        const query = q == null ? this.defaultQuery : q === '' ? '' : q;
        const branch = params.get('b') || '';
        const assignment = params.get('a') || '';
        const comment = params.get('c') || '';
        const numberOfCommits = Number(params.get('n')) || 0;
        return {
          filter: query,
          branch,
          assignment,
          commentFilter: comment,
          numberOfCommits,
        };
      })
    );
  }

  navigateTo(id: string) {
    this.router.navigate(['/testresult-detail'], { queryParams: { id } }).catch((err) => {
      errorSnackBar(this.snackBar, err);
    });
  }

  onResultClicked(hits: Hit<Testresult>[]) {
    this.dialogRef = this.dialog.open(TestEnvironmentResultListComponent, {
      data: hits,
    });

  }

  onFilter(filter: Filter) {
    const queryParams: Params = { q: filter.filter };

    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParamsHandling: 'merge',
      queryParams,
    }).catch((err) => {
      errorSnackBar(this.snackBar, err)
    });
  }

  private defaultQuery = 'start-time:[now-7d/d TO *]';
}
