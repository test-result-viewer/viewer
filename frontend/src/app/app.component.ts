import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as utils from './shared/utilities/utilities';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private route: Router) {}
  title = 'angular-frontend';
  pageTitle = '';

  setTitle() {
    const path = this.route.url.split('/')[1].split('?')[0];
    this.pageTitle = utils.toTitleCase(decodeURIComponent(path));
  }
}
