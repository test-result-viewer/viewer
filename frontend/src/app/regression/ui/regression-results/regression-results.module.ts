import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegressionResultsComponent } from './regression-results.component';

@NgModule({
  declarations: [RegressionResultsComponent],
  imports: [CommonModule],
  exports: [RegressionResultsComponent],
})
export class RegressionResultsModule {}
