import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegressionResultsComponent } from './regression-results.component';

describe('RegressionResultsComponent', () => {
  let component: RegressionResultsComponent;
  let fixture: ComponentFixture<RegressionResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegressionResultsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RegressionResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
