import { Component, Input } from '@angular/core';
import { Regression } from 'src/app/types/testresults.types';

@Component({
  selector: 'app-regression-results',
  templateUrl: './regression-results.component.html',
  styleUrls: ['./regression-results.component.scss'],
})
export class RegressionResultsComponent {
  failedRuns = [] as Regression[];

  @Input()
  set results(failed: Regression[]) {
    this.failedRuns = failed;
  }
}
