import { Injectable } from '@angular/core';
import { Observable, catchError, map } from 'rxjs';
import { Hit, Bucket, SearchResponse } from 'src/app/types/elasticsearch.types';
import { Testresult, Regression } from 'src/app/types/testresults.types';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { HttpErrorResponse } from '@angular/common/http';

interface AggregationBucket extends Bucket {
  subAgg: {
    hits: {
      total: {
        value: number;
        relation: string;
      };
      max_score: number | null;
      hits: Hit<Testresult>[];
    };
  };
}

interface AggregationQueryResponse extends SearchResponse {
  aggregations: {
    [firstAgg: string]: {
      doc_count_error_upper_bound: number;
      sum_other_doc_count: number;
      buckets: AggregationBucket[];
    };
  };
}

@Injectable({
  providedIn: 'root',
})
export class RegressionService {
  constructor(private elasticsearch: ElasticsearchService) {}

  getRegression(
    noOfDays: number,
    returnedFields: string[]
  ): Observable<Regression[]> {
    const result = this.elasticsearch
      .templateSearch<AggregationQueryResponse>(
        'testresult',
        'get-regression',
        { returnedFields: returnedFields }
      )
      .pipe(
        map((response: AggregationQueryResponse) => {
          return response.aggregations['firstAgg'].buckets.map(
            (bucket: AggregationBucket) => {
              return {
                key: bucket.key,
                runs: bucket['subAgg'].hits.hits.map((hit: Hit<Testresult>) => {
                  return hit._source;
                }),
              };
            }
          );
        }),
        map(buckets => {
          return buckets.filter(bucket => {
            return bucket.runs[0].result === 'failure';
          });
        }),
        map(buckets => {
          return buckets.filter(bucket => {
            if (bucket.runs[0]['start-time']) {
              const bucketDate = new Date(bucket.runs[0]['start-time']);
              const ms =
                new Date().getTime() - noOfDays * (24 * 60 * 60 * 1000);
              return bucketDate.getTime() >= ms;
            } else return true;
          });
        }),
        catchError((err: HttpErrorResponse) => {
          throw err.message;
        })
      );
    return result;
  }
}
