import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RegressionComponent } from './regression.component';
import { RegressionService } from '../data-access/regression.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

describe('RegressionComponent', () => {
  let component: RegressionComponent;
  let fixture: ComponentFixture<RegressionComponent>;
  let regressionService: RegressionService;
  let getRegressionSpy: jasmine.Spy;

  let getRegressionSampleData = [
    {
      key: 'test case 20',
      runs: [
        {
          'start-time': '2023-02-08T20:12:32.000000Z',
          build: {
            identifier: 'build id',
            brand: '',
            number: 0,
            project: '',
            trigger: 'commit',
            url: '',
          },
          'version-control-system-info': [
            {
              branch: 'test',
              'commit-hash': 'e0ca0de9576ff2b52a265f574258ddf791491c2b',
              'commit-time': '2023-04-21T11:04:59.697375Z',
              'commit-author': 'someone@sonova.com',
              'commit-message': 'bugfix',
            },
            {
              branch: 'branch2',
              'commit-hash': '3b566d99ecaf039fa154348a3bfa36e451512da3',
              'commit-time': '2023-05-02T00:31:50.396150Z',
              'commit-author': 'someone@sonova.com',
              'commit-message': 'bugfix',
            },
          ],
          result: 'failure',
        },
      ],
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegressionComponent],
      providers: [
        { provide: HttpClient, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(RegressionComponent);
    component = fixture.componentInstance;

    regressionService = fixture.debugElement.injector.get(RegressionService);
    getRegressionSpy = spyOn(
      regressionService,
      'getRegression'
      ).and.returnValue(of(getRegressionSampleData));
    
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getRegression', () => {
    expect(getRegressionSpy).toHaveBeenCalled();
  });
});
