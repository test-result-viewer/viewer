import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegressionComponent } from './regression.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RegressionRoutingModule } from './regression-routing.module';
import { RegressionResultsModule } from '../ui/regression-results/regression-results.module';

@NgModule({
  declarations: [RegressionComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    RegressionRoutingModule,
    RegressionResultsModule,
  ],
})
export class RegressionModule {}
