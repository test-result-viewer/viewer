import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegressionComponent } from './regression.component';

const routes: Routes = [
  {
    path: '',
    component: RegressionComponent,
    title: 'Regression - Test Result Viewer',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegressionRoutingModule {}
