import { Component, OnInit } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { Regression, Testresult } from 'src/app/types/testresults.types';
import { RegressionService } from '../data-access/regression.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { errorSnackBar } from 'src/app/shared/utilities/utilities';

const DEFAULT_DATA = [
  {
    key: 'no data',
    runs: [] as Testresult[],
  },
];

@Component({
  selector: 'app-regression',
  templateUrl: './regression.component.html',
  styleUrls: ['./regression.component.scss'],
})
export class RegressionComponent implements OnInit {
  failedRuns$: Observable<Regression[]> = of(DEFAULT_DATA);
  returnedFields = [
    'testcase',
    'result',
    'build.identifier',
    'version-control-system-info.repo',
    'version-control-system-info.branch',
    'version-control-system-info.commit-hash',
    'version-control-system-info.commit-time',
    'version-control-system-info.commit-author',
    'version-control-system-info.commit-message',
    'start-time',
  ];

  constructor(
    private service: RegressionService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.failedRuns$ = this.service.getRegression(7, this.returnedFields).pipe(
      catchError((msg: string) => {
        errorSnackBar(this.snackBar, msg);
        return of(DEFAULT_DATA);
      })
    );
  }
}
