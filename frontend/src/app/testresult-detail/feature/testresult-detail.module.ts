import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { TestresultDetailRoutingModule } from './testresult-detail-routing.module';
import { TestresultDetailComponent } from './testresult-detail.component';
import { TestresultDetailLabelFormModule } from '../ui/testresult-detail-label-form/testresult-detail-label-form.module';
import { TestresultDetailTableModule } from '../ui/testresult-detail-table/testresult-detail-table.module';

@NgModule({
  declarations: [TestresultDetailComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    TestresultDetailRoutingModule,
    TestresultDetailLabelFormModule,
    TestresultDetailTableModule,
  ],
  exports: [TestresultDetailComponent],
})
export class TestresultDetailModule {}
