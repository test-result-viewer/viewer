import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, map, of } from 'rxjs';
import { Testresult } from 'src/app/types/testresults.types';
import { TestresultDetailService } from '../data-access/testresult-detail.service';
import { Hit, UpdateResponse } from 'src/app/types/elasticsearch.types';
import {
  LabelEvent,
  TestresultDetailLabelFormComponent,
} from '../ui/testresult-detail-label-form/testresult-detail-label-form.component';

@Component({
  selector: 'app-testresult-detail',
  templateUrl: './testresult-detail.component.html',
  styleUrls: ['./testresult-detail.component.scss'],
})
export class TestresultDetailComponent implements OnInit {
  document$!: Observable<Hit<Testresult>>; // TODO: Fix !
  documentId$: Observable<string> = of('');
  updateDocument$!: Observable<UpdateResponse>; // TODO: fix !
  showLabelForm = false;

  @ViewChild(TestresultDetailLabelFormComponent, { static: false })
  set lableForm(form: TestresultDetailLabelFormComponent) {
    this._labelFormComponent = form;
  }

  constructor(
    private service: TestresultDetailService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.documentId$ = this.getDocumentId();
    this.updateDocument$ = this.service.getUpdateDocument(this._clickStream$);
    this.document$ = this.service.getDocument(
      this.documentId$,
      this.updateDocument$
    );
  }

  getDocumentId(): Observable<string> {
    return this.route.queryParamMap.pipe(map(params => params.get('id') || ''));
  }

  addLabel() {
    if (!this._labelFormComponent) {
      // TODO: Throw / handle error
      return;
    }
    this.showLabelForm = !this.showLabelForm;
    this._labelFormComponent.label = { key: '', value: '' };
  }

  updateLabel(event: LabelEvent) {
    this.showLabelForm = false;
    this._clickStream$.next({
      id: event.id,
      source: event.source,
      params: event.params,
    });
  }

  editLabel(event: { key: string; value: string }): void {
    if (!this._labelFormComponent) {
      // TODO: Throw error
      return;
    }
    this._labelFormComponent.label = { key: event.key, value: event.value };
    this._labelFormComponent.isEditMode = true;
    this.showLabelForm = true;
    window.scroll(0, 0);
  }

  private _clickStream$ = new Subject<{
    id: string;
    source: string;
    params: unknown;
  }>();
  private _labelFormComponent?: TestresultDetailLabelFormComponent;
}
