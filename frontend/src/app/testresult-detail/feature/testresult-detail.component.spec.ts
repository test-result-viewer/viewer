import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestresultDetailComponent } from './testresult-detail.component';
import { TestresultDetailService } from '../data-access/testresult-detail.service';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { By } from '@angular/platform-browser';
import { Testresult } from 'src/app/types/testresults.types';
import { Hit } from 'src/app/types/elasticsearch.types';

describe('TestresultDetailComponent', () => {
  let component: TestresultDetailComponent;
  let fixture: ComponentFixture<TestresultDetailComponent>;
  let testresultDetailService: TestresultDetailService;
  let getUpdateDocumentSpy: jasmine.Spy;
  let getDocument: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestresultDetailComponent],
      providers: [
        { provide: HttpClient, useValue: {} },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParamMap: of({ get: (_: string) => 'an-awsome-id' }),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestresultDetailComponent);
    component = fixture.componentInstance;

    testresultDetailService = fixture.debugElement.injector.get(
      TestresultDetailService
    );
    getDocument = spyOn(testresultDetailService, 'getDocument').and.returnValue(
      of({
        _id: 'an-awsome-id',
        _index: 'testrestult',
        _score: 1,
        _source: {
          testcase: 'some test case',
        },
      })
    );
    getUpdateDocumentSpy = spyOn(
      testresultDetailService,
      'getUpdateDocument'
    ).and.returnValue(
      of({
        _id: 'an-awsome-id',
        _index: 'testresult',
        _version: 2,
        result: 'success',
        _seq_no: 2,
        _primary_term: 2,
        _shards: {
          total: 1,
          successful: 1,
          skipped: 0,
          failed: 0,
        },
      })
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should blub', (done: DoneFn) => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.showLabelForm).toBeFalse();
      fixture.debugElement
        .query(By.css('#show-label-form-button'))
        .nativeElement.click();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        // expect(component.showLabelForm).toBeTrue(); // TODO: Fix this test
        done();
      });
    });
  });
});
