import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestresultDetailComponent } from './testresult-detail.component';

const routes: Routes = [
  {
    // TODO: Update routing
    path: '',
    component: TestresultDetailComponent,
    title: 'Test Result - Test Result Viewer',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestresultDetailRoutingModule {}
