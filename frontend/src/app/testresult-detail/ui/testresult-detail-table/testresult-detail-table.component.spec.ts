import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestresultDetailTableComponent } from './testresult-detail-table.component';
import { Testresult } from 'src/app/types/testresults.types';
import { By } from '@angular/platform-browser';

describe('TestresultDetailTableComponent', () => {
  let component: TestresultDetailTableComponent;
  let fixture: ComponentFixture<TestresultDetailTableComponent>;
  let clickEventSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestresultDetailTableComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestresultDetailTableComponent);
    component = fixture.componentInstance;

    clickEventSpy = spyOn(component.clickEvent, 'emit');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event when click on label edit button', (done: DoneFn) => {
    const testresult: Testresult = {
      labels: [{ key: 'my-key', value: 'my-value' }],
    };

    component.testresult = testresult;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.debugElement.query(By.css('button')).nativeElement.click();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(clickEventSpy.calls.count()).toBe(1);
        expect(clickEventSpy.calls.first().args.length).toBe(1);
        expect(clickEventSpy.calls.first().args[0]).toEqual({
          key: testresult.labels![0].key,
          value: testresult.labels![0].value,
        });
        done();
      });
    });
  });
});
