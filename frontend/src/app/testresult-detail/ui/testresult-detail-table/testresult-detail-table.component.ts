import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Testresult } from 'src/app/types/testresults.types';

@Component({
  selector: 'app-testresult-detail-table',
  templateUrl: './testresult-detail-table.component.html',
  styleUrls: ['./testresult-detail-table.component.scss'],
})
export class TestresultDetailTableComponent {
  @Input() testresult: Testresult = {};
  @Output() clickEvent = new EventEmitter<{ key: string; value: string }>();

  onClick(key: string, value: string): void {
    this.clickEvent.emit({ key, value });
  }
}
