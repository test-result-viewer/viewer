import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { TestresultDetailTableComponent } from './testresult-detail-table.component';

@NgModule({
  declarations: [TestresultDetailTableComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [TestresultDetailTableComponent],
})
export class TestresultDetailTableModule {}
