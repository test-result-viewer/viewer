import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestresultDetailLabelFormComponent } from './testresult-detail-label-form.component';
import { By } from '@angular/platform-browser';
import { Expression } from '@angular/compiler';

describe('TestresultDetailLabelFormComponent', () => {
  let component: TestresultDetailLabelFormComponent;
  let fixture: ComponentFixture<TestresultDetailLabelFormComponent>;
  let updateLabelEventSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestresultDetailLabelFormComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestresultDetailLabelFormComponent);
    component = fixture.componentInstance;

    updateLabelEventSpy = spyOn(component.updateLabelEvent, 'emit');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update form when label is updated', (done: DoneFn) => {
    const id = 'some id';
    const label = { key: 'a key', value: 'a value' };

    component.documentId = id;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.id?.value).toEqual('some id');
      expect(component.key?.value).toEqual('');
      expect(component.value?.value).toEqual('');

      component.label = label;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.id?.value).toEqual('some id');
        expect(component.key?.value).toEqual(label.key);
        expect(component.value?.value).toEqual(label.value);
        done();
      });
    });
  });

  it('should emit event when add is triggered', (done: DoneFn) => {
    const id = 'some id';
    const label = { key: 'a key', value: 'a value' };
    component.documentId = id;
    component.label = label;

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const event = new Event('click');
      component.addLabel(event);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(updateLabelEventSpy.calls.count()).toBe(1);
        expect(updateLabelEventSpy.calls.first().args.length).toBe(1);
        expect(updateLabelEventSpy.calls.first().args[0]).toEqual({
          event,
          id,
          source: 'ctx._source.labels.add(params.label)',
          params: { label: { key: label.key, value: label.value } },
        });
        done();
      });
    });
  });

  it('should emit event when update is triggered', (done: DoneFn) => {
    const id = 'some id';
    const label = { key: 'a key', value: 'a value' };
    const old = { key: 'a key', value: 'old value' };
    component.documentId = id;
    component.label = old;
    component.key?.setValue(label.key);
    component.value?.setValue(label.value);

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const event = new Event('click');
      component.updateLabel(event);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(updateLabelEventSpy.calls.count()).toBe(1);
        expect(updateLabelEventSpy.calls.first().args.length).toBe(1);
        expect(updateLabelEventSpy.calls.first().args[0]).toEqual({
          event,
          id,
          source:
            'ctx._source.labels[(ctx._source.labels.indexOf(params.label))] = params.new',
          params: { new: { key: label.key, value: label.value }, old },
        });
        done();
      });
    });
  });

  it('should emit event when delete is triggered', (done: DoneFn) => {
    const id = 'some id';
    const label = { key: 'a key', value: 'a value' };
    component.documentId = id;
    component.label = label;

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const event = new Event('click');
      component.deleteLabel(event);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(updateLabelEventSpy.calls.count()).toBe(1);
        expect(updateLabelEventSpy.calls.first().args.length).toBe(1);
        expect(updateLabelEventSpy.calls.first().args[0]).toEqual({
          event,
          id,
          source:
            'ctx._source.labels.remove(ctx._source.labels.indexOf(params.label))',
          params: { label: { key: label.key, value: label.value } },
        });
        done();
      });
    });
  });
});
