import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EMPTY_LABEL, Label } from 'src/app/types/testresults.types';

export interface LabelEvent {
  event: Event;
  id: string;
  source: string;
  params: unknown;
}

@Component({
  selector: 'app-testresult-detail-label-form',
  templateUrl: './testresult-detail-label-form.component.html',
  styleUrls: ['./testresult-detail-label-form.component.scss'],
})
export class TestresultDetailLabelFormComponent implements OnInit {
  @Input()
  set documentId(i: string) {
    this.id?.setValue(i); // TODO: Make this more pretty
  }

  @Input()
  set label(l: Label) {
    this._label = l;
    this.key?.setValue(l.key);
    this.value?.setValue(l.value);
  }
  @Output() updateLabelEvent = new EventEmitter<LabelEvent>();

  labelForm: FormGroup = new FormGroup([]);
  isEditMode = false;

  ngOnInit(): void {
    this.labelForm = new FormGroup({
      id: new FormControl(this.id),
      key: new FormControl(this._label.key),
      value: new FormControl(this._label.value),
    });
  }

  get id() {
    return this.labelForm.get('id');
  }
  get key() {
    return this.labelForm.get('key');
  }
  get value() {
    return this.labelForm.get('value');
  }

  addLabel(event: Event): void {
    const source = 'ctx._source.labels.add(params.label)';
    const params = {
      label: { key: this.key?.value, value: this.value?.value },
    };
    this.isEditMode = false;
    this.updateLabelEvent.emit({ event, id: this.id?.value, source, params });
  }

  deleteLabel(event: Event): void {
    const source =
      'ctx._source.labels.remove(ctx._source.labels.indexOf(params.label))';
    const params = {
      label: { key: this.key?.value, value: this.value?.value },
    };

    this.isEditMode = false;
    this.updateLabelEvent.emit({ event, id: this.id?.value, source, params });
  }

  updateLabel(event: Event): void {
    const source =
      'ctx._source.labels[(ctx._source.labels.indexOf(params.label))] = params.new';
    const params = {
      new: { key: this.key?.value, value: this.value?.value },
      old: this._label,
    };

    this.isEditMode = false;
    this.updateLabelEvent.emit({ event, id: this.id?.value, source, params });
  }

  private _label = EMPTY_LABEL;
}
