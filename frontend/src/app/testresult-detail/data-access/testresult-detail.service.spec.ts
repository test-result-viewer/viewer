import { TestBed } from '@angular/core/testing';

import { TestresultDetailService } from './testresult-detail.service';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { catchError, of, take, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UpdateResponse } from 'src/app/types/elasticsearch.types';

describe('TestresultDetailService', () => {
  let service: TestresultDetailService;
  let elasticsearchService: ElasticsearchService;
  let getByIdSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: {} }],
    });
    service = TestBed.inject(TestresultDetailService);

    elasticsearchService = TestBed.inject(ElasticsearchService);
    getByIdSpy = spyOn(
      elasticsearchService,
      'getById'
    ).and.returnValue(of({
        "_index": "testresult",
        "_id": "LuZHv4gBZm6mj1uDrKAC",
        "_version": 1,
        "_seq_no": 571,
        "_primary_term": 1,
        "found": true,
        "_source": {
          "version-control-system-info": [
            {
              "commit-hash": "7c532397ae03a28474cd64664f86b58624b40576",
              "commit-author": "darek@sonova.com",
              "commit-message": "bugfix again",
              "repository": "reponame",
              "type": "svn",
              "branch": "dev",
              "commit-time": "2023-06-15T07:38:54.239025Z"
            },
            {
              "commit-hash": "1331d7552c38b4ebc8e3e840384f792b923d10a6",
              "commit-author": "thomas@sonova.com",
              "commit-message": "added some feature",
              "repository": "somerepo",
              "type": "git",
              "branch": "branch2",
              "commit-time": "2023-05-14T12:58:21.319595Z"
            }
          ],
          "reason": "",
          "start-time": "2023-06-16T09:00:00.000000Z",
          "testsuite": "suite1",
          "culture-info": {
            "current-uiculture": "en-US",
            "current-culture": "de-CH"
          },
          "description": "some description to be randomized",
          "executed": true,
          "testcase": "test case 13",
          "testassembly": "another.dll",
          "labels": [],
          "result": "success",
          "asserts": 0,
          "environment": {
            "os-version": "Microsoft Windows NT 6.2.9200.0",
            "cwd": "C:\\test\\directory",
            "framework": "nunit",
            "user-domain": "domain3",
            "framework-version": "2.6.4.20092",
            "clr-version": "4.0.12345.2100",
            "user": "Andreas",
            "platform": "UNIX",
            "machine-name": "Server123"
          },
          "build": {
            "identifier": "build id",
            "number": 36,
            "project": "project",
            "trigger": "trigger",
            "brand": "brand",
            "url": "url"
          },
          "failure": {
            "stacktrace": "",
            "message": ""
          },
          "time": 8.002,
          "categories": [
            "HealthChecks"
          ],
          "test-environment": {
            "capabilities": [
              "label #2"
            ],
            "name": "host-A"
          },
          "properties": [
            {
              "name": "property2",
              "value": "someothervalue"
            },
            {
              "name": "propery1",
              "value": "false"
            }
          ]
        }
      }));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the document', (done: DoneFn) => {
    getByIdSpy.calls.reset();
    service.getDocument(of('LuZHv4gBZm6mj1uDrKAC'), of({} as unknown as UpdateResponse)).pipe(take(1)).subscribe(r => {
      expect(getByIdSpy.calls.mostRecent().args).toEqual(['testresult', 'LuZHv4gBZm6mj1uDrKAC']);
      expect(r._id).toBe('LuZHv4gBZm6mj1uDrKAC');
      expect(r._source.testcase).toBe('test case 13');
      done();
    });
  });
});

describe('TestresultDetailService with HTTP error', () => {
  let service: TestresultDetailService;
  let elasticsearchService: ElasticsearchService;
  let getByIdSpy: jasmine.Spy;
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpClientSpy }],
    });
    httpClientSpy.get.and.returnValue(throwError(() => new Error()));
    httpClientSpy.post.and.returnValue(throwError(() => new Error()));
    elasticsearchService = TestBed.inject(ElasticsearchService);

    service = TestBed.inject(TestresultDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should do nothing on error', () => {
    service.getDocument(of('LuZHv4gBZm6mj1uDrKAC'), of({} as unknown as UpdateResponse))
    .pipe(
      take(1),
      catchError(r => 'e')
    )
    .subscribe(r => {
      expect(r).toBe('e')
    });
  });
});
