import { Injectable } from '@angular/core';
import { Observable, catchError, map, merge, switchMap } from 'rxjs';
import { Hit, UpdateResponse } from 'src/app/types/elasticsearch.types';
import { Testresult } from 'src/app/types/testresults.types';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { HttpErrorResponse } from '@angular/common/http';

const TESTRESULT_INDEX = 'testresult';

@Injectable({
  providedIn: 'root',
})
export class TestresultDetailService {
  constructor(private elasticsearch: ElasticsearchService) {}

  getDocument(
    documentId$: Observable<string>,
    updateDocument$: Observable<UpdateResponse>
  ): Observable<Hit<Testresult>> {
    return merge(
      documentId$,
      updateDocument$.pipe(
        map(response => response._id)
      )
    ).pipe(
      switchMap(documentId =>
        this.elasticsearch
          .getById<Hit<Testresult>>(TESTRESULT_INDEX, documentId)
          .pipe(
            catchError((err: HttpErrorResponse) => {
              throw err.message;
            })
          )
      )
    );
  }

  getUpdateDocument(
    clickObservable$: Observable<{
      id: string;
      source: string;
      params: unknown;
    }>
  ): Observable<UpdateResponse> {
    return clickObservable$.pipe(
      switchMap(({ id, source, params }) => {
        const doc = {
          script: {
            source,
            lang: 'painless',
            params,
          },
        };
        return this.elasticsearch.update(TESTRESULT_INDEX, id, doc).pipe(
          catchError((err: HttpErrorResponse) => {
            throw err.message;
          })
        );
      })
    );
  }
}
