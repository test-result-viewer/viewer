import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule } from '@angular/material/expansion';
import { DatabaseSchemaSnippetComponent } from './database-schema-snippet.component';

@NgModule({
  declarations: [DatabaseSchemaSnippetComponent],
  imports: [
    CommonModule,
    MatExpansionModule
  ],
  exports: [DatabaseSchemaSnippetComponent],
})
export class DatabaseSchemaSnippetModule {}
