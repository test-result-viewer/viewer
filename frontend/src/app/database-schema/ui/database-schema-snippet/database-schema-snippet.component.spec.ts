import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabaseSchemaSnippetComponent } from './database-schema-snippet.component';

describe('DatabaseSchemaSnippetComponent', () => {
  let component: DatabaseSchemaSnippetComponent;
  let fixture: ComponentFixture<DatabaseSchemaSnippetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DatabaseSchemaSnippetComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DatabaseSchemaSnippetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
