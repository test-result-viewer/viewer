import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-database-schema-snippet',
  templateUrl: './database-schema-snippet.component.html',
  styleUrls: ['./database-schema-snippet.component.scss'],
})
export class DatabaseSchemaSnippetComponent {
  // TODO: Update type
  @Input() schema: { index: string; fields: object } = {
    index: '',
    fields: {},
  };

  panelOpenState = false;
}
