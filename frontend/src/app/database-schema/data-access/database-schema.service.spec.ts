import { TestBed } from '@angular/core/testing';

import { DatabaseSchemaService } from './database-schema.service';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

describe('DatabaseSchemaService', () => {
  let service: DatabaseSchemaService;
  let elasticsearchService: ElasticsearchService;
  let getAllIndicesSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
      ],
    });

    const schemaResponse = {
      testresult: {
        aliases: {},
        mappings: {
          properties: {
            asserts: {
              type: 'text',
              fields: {
                keyword: {
                  type: 'keyword',
                  ignore_above: 256,
                },
              },
            },
            build: {
              properties: {
                brand: {
                  type: 'text',
                  fields: {
                    keyword: {
                      type: 'keyword',
                      ignore_above: 256,
                    },
                  },
                },
                identifier: {
                  type: 'text',
                  fields: {
                    keyword: {
                      type: 'keyword',
                      ignore_above: 256,
                    },
                  },
                },
              },
            },
          },
        },
      },
    };

    elasticsearchService = TestBed.inject(ElasticsearchService);
    getAllIndicesSpy = spyOn(
      elasticsearchService,
      'getAllIndices'
    ).and.returnValue(of(schemaResponse));
    service = TestBed.inject(DatabaseSchemaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the schemas as JSON', (done: DoneFn) => {
    service.getAllSchemas().subscribe(schemas => {
      expect(schemas.length).toBe(1);
      expect(schemas[0].index).toEqual('testresult');
      expect(schemas[0].fields).toEqual({
        asserts: {
          type: 'text',
          keyword: { type: 'keyword' },
        },
        build: {
          brand: {
            type: 'text',
            keyword: { type: 'keyword' },
          },
          identifier: {
            type: 'text',
            keyword: { type: 'keyword' },
          },
        },
      });
      done();
    });
  });
});
