import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map } from 'rxjs';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { IndexProperty } from 'src/app/types/elasticsearch.types';

export interface Schema {
  index: string;
  fields: object;
}

@Injectable({
  providedIn: 'root',
})
export class DatabaseSchemaService {
  constructor(private elasticsearch: ElasticsearchService) {}

  getAllSchemas(): Observable<Schema[]> {
    return this.elasticsearch.getAllIndices().pipe(
      map(response => {
        const indices = Object.keys(response);

        return indices.map(index => {
          const keys = Object.keys(response[index].mappings?.properties || {});
          const fields: { [x: string]: object } = {};
          keys.forEach(key => {
            fields[key] = this.convert(
              response[index].mappings?.properties[key]
            );
          });

          return { index, fields };
        });
      }),
      catchError((err: HttpErrorResponse) => {
        throw err.message;
      })
    );
  }

  private convert(property: IndexProperty | undefined): object {
    if (property === undefined) return {};

    const fieldKeys = Object.keys(property.fields || {});
    const subFields: { [x: string]: object } = {};

    fieldKeys.forEach(key => {
      subFields[key] = this.convert(property.fields?.[key]);
    });

    const propertyKeys = Object.keys(property.properties || {});
    propertyKeys.forEach(key => {
      subFields[key] = this.convert(property.properties?.[key]);
    });

    if (property.type) {
      return {
        type: property.type,
        ...subFields,
      };
    }

    return {
      ...subFields,
    };
  }
}
