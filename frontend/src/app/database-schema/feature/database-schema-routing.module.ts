import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatabaseSchemaComponent } from './database-schema.component';

const routes: Routes = [
  {
    path: '',
    component: DatabaseSchemaComponent,
    title: 'Database Schema',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatabaseSchemaRoutingModule {}
