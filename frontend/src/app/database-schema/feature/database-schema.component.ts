import { Component, OnInit } from '@angular/core';
import {
  DatabaseSchemaService,
  Schema,
} from '../data-access/database-schema.service';
import { Observable, catchError, of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { errorSnackBar } from 'src/app/shared/utilities/utilities';

const DEFAULT_DATA = [{ index: '', fields: {} as object }];

@Component({
  selector: 'app-database-schema',
  templateUrl: './database-schema.component.html',
  styleUrls: ['./database-schema.component.scss'],
})
export class DatabaseSchemaComponent implements OnInit {
  schemas$: Observable<Schema[]> = of(DEFAULT_DATA);

  constructor(
    private service: DatabaseSchemaService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.schemas$ = this.service.getAllSchemas().pipe(
      catchError((msg: string) => {
        errorSnackBar(this.snackBar, msg);
        return of(DEFAULT_DATA);
      })
    );
  }
}
