import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { DatabaseSchemaRoutingModule } from './database-schema-routing.module';
import { DatabaseSchemaComponent } from './database-schema.component';
import { DatabaseSchemaSnippetModule } from '../ui/database-schema-snippet/database-schema-snippet.module';

@NgModule({
  declarations: [DatabaseSchemaComponent],
  imports: [
    CommonModule,
    MatSnackBarModule,
    DatabaseSchemaRoutingModule,
    DatabaseSchemaSnippetModule,
  ],
  exports: [DatabaseSchemaComponent],
})
export class DatabaseSchemaModule {}
