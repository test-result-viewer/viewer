import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabaseSchemaComponent } from './database-schema.component';
import { ElasticsearchService } from 'src/app/shared/data-access/elasticsearch/elasticsearch.service';
import { DatabaseSchemaService } from '../data-access/database-schema.service';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

describe('DatabaseSchemaComponent', () => {
  let component: DatabaseSchemaComponent;
  let fixture: ComponentFixture<DatabaseSchemaComponent>;

  let databaseSchemaService: DatabaseSchemaService;
  let getAllSchemasSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DatabaseSchemaComponent],
      providers: [
        { provide: ElasticsearchService, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DatabaseSchemaComponent);
    component = fixture.componentInstance;

    databaseSchemaService = fixture.debugElement.injector.get(
      DatabaseSchemaService
    );
    getAllSchemasSpy = spyOn(
      databaseSchemaService,
      'getAllSchemas'
    ).and.returnValue(
      of([
        {
          index: 'testresult',
          fields: {
            asserts: {
              type: 'integer',
            },
            build: {
              brand: {
                type: 'text',
                keyword: {
                  type: 'keyword',
                },
              },
            },
          },
        },
      ])
    );
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should query index schemas', () => {
    component.schemas$.subscribe(s => {
      expect(s.length).toBe(1);
      expect(s[0].index).toEqual('testresult');
      expect(s[0].fields).toEqual({
        asserts: {
          type: 'integer',
        },
        build: {
          brand: {
            type: 'text',
            keyword: {
              type: 'keyword',
            },
          },
        },
      });
    });
  });
});
